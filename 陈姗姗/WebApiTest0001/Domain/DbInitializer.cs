﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Transactions;
using WebApiTest0001.Domain.Entity;

namespace WebApiTest0001.Domain
{
    public class DbInitializer
    {

        public static void Seed()
        {
            using var db = new Admin0001DbContext();
            db.Database.EnsureCreated();

            var hasUser = db.Users.Any();

            if (!hasUser)
            {
                var role = new Roles
                {
                    RoleName = "豌豆",
                    Description = "这是一个豌豆射手，这是打僵尸的豌豆射手！！！！"
                };

                db.Roles.Add(role);
                db.SaveChanges();

                db.Users.AddRange(new Users[]
                {
                    new Users
                    {
                         Username="admin",
                         Password="admid",
                         RolesId=role.Id
                    },
                   new Users
                   {
                         Username="爆炸樱桃",
                         Password="12345",
                         RolesId=role.Id
                   }
                });

                db.SaveChanges();
            }
        }
    }
}
