﻿using System.Collections.Generic;

namespace WebApiTest0001.Domain.Entity
{
    public class Roles : BaseEntity
    {
        
        public  string RoleName { get; set; }

        public string Description { get; set; }

        public IEnumerable<Users> Users { get; set; }

        //public virtual int Add(int a,int b)
        //{
        //    return a + b;
        //}
        
    }

    ////定义了一个抽象类的方法
    //public abstract class AA
    //{
    //    public abstract int Add(double a, double b);
    //}
}