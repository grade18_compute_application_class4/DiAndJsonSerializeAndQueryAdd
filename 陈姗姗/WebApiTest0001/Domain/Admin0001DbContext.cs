﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiTest0001.Domain.Entity;

namespace WebApiTest0001.Domain
{
    public class Admin0001DbContext:DbContext
    {
        public Admin0001DbContext()
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //与数据库建立连接
            var connectionString = "server=.;database=Admin0001;uid=sa;pwd=123456";
            optionsBuilder.UseSqlServer(connectionString);
        }

        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }
    }

}
