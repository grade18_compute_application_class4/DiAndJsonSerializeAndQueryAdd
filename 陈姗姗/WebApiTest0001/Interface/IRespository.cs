﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiTest0001.Interface
{
   public interface IRespository<T>
    {
        IQueryable<T> Table { get; }

        T GetById(int id);

        //IEnumerable<T> GetAllEntity();

        //添加
        void Insert(T entity);

        void InsertBulk(IEnumerable<T> list);//批量插入

        //更新，修改
        void Update(T entity);

        void Delete(T entity);

        void Delete(int id);//根据Id删除
    }
}
