﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiTest0001.Domain.Entity;
using WebApiTest0001.Helper;
using WebApiTest0001.Interface;

namespace WebApiTest0001.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRespository<Users> _userRespository;

        public TestController(IRespository<Users> userRespository)
        {
            _userRespository = userRespository;
        }
        
        public string Get()
        {
            var list = _userRespository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}