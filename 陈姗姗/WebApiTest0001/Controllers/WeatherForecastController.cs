﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApiTest0001.Domain;
using WebApiTest0001.Domain.Entity;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using WebApiTest0001.Helper;

namespace WebApiTest0001.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly Admin0001DbContext _db;
        
        //1
        //没有引用using Microsoft.Extensions.DependencyInjection;，和输入下面这个，运行的时候就没有结果
        //[ActivatorUtilitiesConstructor]


        //2
        //public WeatherForecastController(Admin0001DbContext dbContext)
        //{
        //    _db = dbContext;
        //}

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;


        //当前版本不能支持两个，所以要注释掉一个
        public WeatherForecastController(ILogger<WeatherForecastController> logger,Admin0001DbContext dbContext)
        {
            _logger = logger;
            _db = dbContext;
        }

        [HttpGet]
        public string Get()
        {
            //var rng = new Random();
            //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            //{
            //    Date = DateTime.Now.AddDays(index),
            //    TemperatureC = rng.Next(-20, 55),
            //    Summary = Summaries[rng.Next(Summaries.Length)]
            //})
            //.ToArray();
            
            
            //使浏览器那边变得美观，数据排列整齐，便与观察。
            var res = _db.Users.Include(x=>x.Roles).ToList();



            return JsonHelper.SerializeObject(res);
        }
    }
}
