﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI10086.Domain.Entity;

namespace WebAPI10086.Controllers
{
    public class DbInitializeHelper
    {
        public static void Initilizer()
        {
            using (var db = new Admin10086DbContext())
            {
                db.Database.EnsureCreated();

                var tempBrand = db.Purchases.Any();
                if (!tempBrand)
                {
                    var Purchase1 = new Purchase
                    {
                        CommodityName = "鸿星尔克",
                        UnitPrice = 156.9M,
                        Quantuty = 100
                    };

                    var Purchase2 = new Purchase
                    {
                        CommodityName = "耐克",
                        UnitPrice = 140.88M,
                        Quantuty = 100
                    };
                    db.Purchases.AddRange(Purchase1, Purchase2);

                    db.SaveChanges();

                    var Maret1 = new Market
                    {

                        CommodityName = Purchase1.CommodityName,
                        UnitPrice = 256.9M,
                        Quantuty = 60

                    };

                    var Maret2 = new Market
                    {
                        CommodityName = Purchase2.CommodityName,
                        UnitPrice = 340.88M,
                        Quantuty = 50
                    };

                    db.MarKets.AddRange(Maret1, Maret2);

                    db.SaveChanges();

                    db.Brands.AddRange(new Brand[]
                    {
                    new Brand
                    {
                        CommodityName = Purchase1.CommodityName,
                        PurchaseQuantuty = Purchase1.Quantuty,
                        PurchaseMoney = Purchase1.Quantuty * Purchase1.UnitPrice,
                        MarketQuantuty = Maret1.Quantuty,
                        MarketMoney = Maret1.Quantuty * Maret1.UnitPrice,
                        BalanceQuantuty = Purchase1.Quantuty - Maret1.Quantuty,
                        RetaubedProFits = (Maret1.Quantuty * Maret1.UnitPrice) - (Purchase1.Quantuty * Purchase1.UnitPrice),
                        PurchaseId = Purchase1.Id,
                        MarKetId = Maret1.Id
                    },new Brand
                    {
                        CommodityName = Purchase2.CommodityName,
                        PurchaseQuantuty = Purchase2.Quantuty,
                        PurchaseMoney = Purchase2.Quantuty * Purchase2.UnitPrice,
                        MarketQuantuty = Maret2.Quantuty,
                        MarketMoney = Maret2.Quantuty * Maret2.UnitPrice,
                        BalanceQuantuty = Purchase2.Quantuty - Maret2.Quantuty,
                        RetaubedProFits = (Maret2.Quantuty * Maret2.UnitPrice) - (Purchase2.Quantuty * Purchase2.UnitPrice),
                        PurchaseId = Purchase2.Id,
                        MarKetId = Maret2.Id
                    }
                    }); ;

                    db.SaveChanges();

                }
            }
                
        }
    }
}
