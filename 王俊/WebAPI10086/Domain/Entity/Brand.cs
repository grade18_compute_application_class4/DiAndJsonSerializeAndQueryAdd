﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI10086.Domain.Entity;

namespace WebAPI10086
{
    public class Brand : BaseEntity
    {
        public string CommodityName { get; set; }

        public int PurchaseQuantuty { get; set; } //进货总量

        public decimal PurchaseMoney { set; get; } // 进货总金额

        public int MarketQuantuty { get; set; }  //售出总量

        public decimal MarketMoney { set; get; } // 售出总金额

        public int BalanceQuantuty { get; set; } //结存数量

        public decimal RetaubedProFits { get; set; } //净利润

        public int PurchaseId { get; set; }

        public int MarKetId { get; set; }

        public Purchase Purchase { get; set; }

        public Market Market { get; set; }
    }
}
