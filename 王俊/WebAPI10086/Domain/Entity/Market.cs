﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI10086.Domain.Entity
{
    public class Market : BaseEntity
    {
        public string CommodityName { get; set; } // 商品名称

        public decimal UnitPrice { set; get; } //单价

        public int Quantuty { set; get; } // 数量


        public int PurchasesId { set; get; }

        public Purchase Purchase { set; get; }

        public IEnumerable<Brand> Brans { get; set; }
    }
}
