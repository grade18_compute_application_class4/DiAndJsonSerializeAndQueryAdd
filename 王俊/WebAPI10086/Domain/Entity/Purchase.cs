﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI10086.Domain.Entity;

namespace WebAPI10086
{
    public class Purchase : BaseEntity
    {
        public string CommodityName { set; get; } // 商品名称

        public decimal UnitPrice { set; get; } //单价

        public int Quantuty { set; get; } // 数量

        public IEnumerable<Brand> Brands { get; set; }

        public IEnumerable<Market> Markets { get; set; }
    }
}


