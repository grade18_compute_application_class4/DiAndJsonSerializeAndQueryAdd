﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI10086
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            IsActived = true;
            IsDeleted = false;
            CreatedTime = DateTime.Now;
            UpdateTime = DateTime.Now;
        }

        public int Id { set; get; }

        public bool IsActived { set; get; }

        public bool IsDeleted { set; get; }

        public DateTime CreatedTime { set; get; }

        public DateTime UpdateTime { set; get; }

        public string Remarks { set; get; }

    }
}

