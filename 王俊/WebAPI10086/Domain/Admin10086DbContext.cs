﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI10086.Domain.Entity;

namespace WebAPI10086
{
    public class Admin10086DbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)//构建器
        {
            optionsBuilder.UseSqlServer("server=DESKTOP-S3544UE\\SQLEXPRESS;database=Admin1008611;uid=sa;pwd = 123456;");
        }

        public DbSet<Market> MarKets { get; set; }

        public DbSet<Purchase> Purchases { get; set; }

        public DbSet<Brand> Brands { get; set; }

    }
}
