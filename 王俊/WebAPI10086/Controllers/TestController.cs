﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI10086.Domain.Helper;
using WebAPI10086.Infterface;

namespace WebAPI10086.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRespository<Brand> _brandRespository;

        public TestController(IRespository<Brand> brandRespository)
        {
            _brandRespository = brandRespository;
        }

        public string Get()
        {
            var list = _brandRespository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }

        [Route("{id}")]
        public string Get(int id)
        {
            var list = _brandRespository.GetById(id);
            return JsonHelper.SerializeObject(list);
        }

        
    }
}