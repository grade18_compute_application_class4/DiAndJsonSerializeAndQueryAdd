﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WebAPI10086.Domain.Helper;

namespace WebAPI10086.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly Admin10086DbContext _db;

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger,Admin10086DbContext dbContext)
        {
            _logger = logger;
            _db = dbContext;
        }

        [HttpGet]
        public string Get()
        {

            //var list = new List<Brand>();
            //using (var db = new Admin10086DbContext()) 
            //{
            //    list = db.Brands.ToList<Brand>();
            //}
            //string str = JsonConvert.SerializeObject(list, Formatting.Indented);
            var str = _db.Brands.Include(x=>x.Purchase).ToList();
            return JsonHelper.SerializeObject(str);
        }
    }
}
 