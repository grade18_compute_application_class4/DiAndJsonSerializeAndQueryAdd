﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI10086.Infterface;

namespace WebAPI10086.Impementation
{
    public class EfRespository<T> : IRespository<T> where T : BaseEntity
    {
        private readonly Admin10086DbContext db;

        private DbSet<T> _entity;

        private DbSet<T> Entity
        {
            get
            {
                if (_entity == null)
                {
                    _entity = db.Set<T>();
                }
                return _entity;
            }
            
        }
        public IQueryable
            <T> Table
        {
            get
            {
                return Entity;
            }
        }


        public EfRespository(Admin10086DbContext dbContext)
        {
            db = dbContext;
        }

        
        public void Delete(T entity)
        {
            this._entity.Remove(entity);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var rew = Table.Where(x => x.Id == id).FirstOrDefault();
            Delete(rew);
            db.SaveChanges();
        }


        public T GetById(int id)
        {
            return Table.Where(x => x.Id == id).FirstOrDefault();
        }

        public void Insert(T entity)
        {
            _entity.Add(entity);
            db.SaveChanges();
        }

        public void InsertBulk(IEnumerable<T> list)
        {
            _entity.AddRange(list);
            db.SaveChanges();
        }

        public void Update(T entity)
        {
            _entity.Update(entity);
            db.SaveChanges();
        }
    }
}
