﻿using DIText001.DOMain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIText001.DOMain
{
    public class Dbinitializer
    {
        public static void Seed()
        {
            using (var db = new Admin3000Context())
            {
                db.Database.EnsureCreated();
                var hasUser = db.Users.Any();
                if (!hasUser)
                {
                    var role1 = new Roles
                    {
                        RoleName = "xxx",
                        Description = "aabb"
                    };
                    db.Roles.Add(role1);
                    db.SaveChanges();
                    db.Users.AddRange(new Users[]
                    {
                        new Users
                        {
                            Username="xxx",
                            Password="123456",
                            RoleId=role1.Id

                        },
                        new Users
                        {
                            Username="xxx",
                            Password="123456",
                            RoleId=role1.Id
                        }
                    });
                }
                db.SaveChanges();
            }
        }
    }
}
