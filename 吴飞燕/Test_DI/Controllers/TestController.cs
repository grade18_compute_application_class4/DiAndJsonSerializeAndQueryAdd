﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Migrations;
using Test_DI.Domain.Entity;
using Test_DI.Helper;
using Test_DI.Interface;

namespace Test_DI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRespository<Users> _userRespository;

        public TestController(IRespository<Users> useRespository)
        {
           _userRespository = useRespository;
        }
        public string Get()
        {
            var list = _userRespository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}
