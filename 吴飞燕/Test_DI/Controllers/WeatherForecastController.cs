﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Test_DI.Domain;
using Microsoft.Extensions.DependencyInjection;
using Test_DI.Domain.Entity;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using Test_DI.Helper;

namespace Test_DI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        /// <summary>
        /// 加的private public
        /// </summary>
        private readonly Admin3200DbContext _db;

        //[ActivatorUtilitiesConstructor]
        //public WeatherForecastController(Admin3200DbContext dbContext)
        //{
        //    _db = dbContext;
        //}

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger,Admin3200DbContext dbContext )
        {
            _logger = logger;
            _db = dbContext;
        }

        [HttpGet]
        public string Get()
        {
            //var rng = new Random();
            //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            //{
            //    Date = DateTime.Now.AddDays(index),
            //    TemperatureC = rng.Next(-20, 55),
            //    Summary = Summaries[rng.Next(Summaries.Length)]
            //})
            //.ToArray();

           var res= _db.Users.Include(x=>x.Roles).ToList();

            return JsonHelper.SerializeObject(res);


            //依赖注入是控制反转的一种实现 
            //属性注入 构造函数注入
        }
    }
}
