﻿using Microsoft.EntityFrameworkCore.Migrations.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test_DI.Interface
{
    public interface IRespository<T>
    {
        IQueryable<T> Table { get; }

        T GetById(int id);

        IEnumerable<T> GetAllEntity();

        int Insert(T entity);

        void InsertBulk(IEnumerable<T> list);

        void Update(T entity);

        void Delete(int Id);

    }
}
