﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test_DI.Domain.Entity;

namespace Test_DI.Domain
{
    public class Admin3200DbContext:DbContext
    {
        public Admin3200DbContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = "server=.;database=Admin3200;uid=sa;pwd=123456";
            optionsBuilder.UseSqlServer(connectionString);
        }

        public DbSet<Users> Users { get; set; }
        public DbSet<Roles> Roles { get; set; }

    }
}
