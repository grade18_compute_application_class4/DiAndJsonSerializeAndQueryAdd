﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test_DI.Domain.Entity
{
    public class Users:BaseEntity
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public int RolesId { get; set; }

        public Roles Roles { get; set; }
    }
}
