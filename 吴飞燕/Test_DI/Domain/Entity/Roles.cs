﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test_DI.Domain.Entity
{
    public class Roles:BaseEntity
    {
        public string RoleName { get; set; }

        public string Description { get; set; }

        public IEnumerable<Users> Users { get; set; }
    }
}
