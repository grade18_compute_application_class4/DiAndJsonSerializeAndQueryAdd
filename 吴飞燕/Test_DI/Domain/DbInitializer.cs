﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test_DI.Domain.Entity;

namespace Test_DI.Domain
{
    public class DbInitializer
    {
        public static void Seed()
        {
            using var db = new Admin3200DbContext();
            db.Database.EnsureCreated();

            var hasUser = db.Users.Any();

            if (!hasUser)
            {
                var role = new Roles
                {
                    RoleName = "派单",
                    Description = "嗯没错，就给你单子做的"
                };
                db.Roles.Add(role);

                db.SaveChanges();
                db.Users.AddRange(new Users[]
                {
                    new Users
                    {
                        Username = "sa",
                        Password= "123456",
                        RolesId=role.Id
                    },
                    new Users
                    {
                        Username = "user01",
                        Password= "12345",
                        RolesId=role.Id
                    }

                });

                db.SaveChanges();

            }

        }
        


    }
}