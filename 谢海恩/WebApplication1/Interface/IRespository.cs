﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Interface
{
    public interface IRespository<T>
    {
        IQueryable<T> Table { get; }
        T GetById(int id);

        //IEnumerable<T> GetAllEntity();

        //添加
        int Insert(T entity);

        //批量插入
        void InsertBulk(IEnumerable<T> list);

        void Update(T entity);

        void Delete(T entity);

        void Delete(int id);
    }
}
