﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Domain;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Interface;

namespace WebApplication1.Implementation
{
    //实现接口
    public class EfRespository<T> : IRespository<T> where T : BaseEntity
    {
        private readonly Admin3200DbContext db;

        private DbSet<T> _entity;

        private DbSet<T> Entity
        {
            get
            {
                if (_entity == null)
                {
                    _entity = db.Set<T>();
                }
                return _entity;
            }
        }

        public IQueryable<T> Table
        {
            get
            {
                return Entity;
            }
        }

        public EfRespository(Admin3200DbContext dbContext)
        {
            db = dbContext; 
        }

        public void Delete(T entity)
        {
            this._entity.Remove(entity);

            db.SaveChanges();//保存更改
        }

        public void Delete(int id)
        {
            var row = Table.Where(x => x.Id == id).FirstOrDefault();
            Delete(row);
        }

        /*public IEnumerable<T> GetAllEntity()
        {
            throw new NotImplementedException();
        }*/

        public T GetById(int id)
        {
            return _entity.Where(x => x.Id == id).FirstOrDefault();
        }

        public void Insert(T entity)
        {
            _entity.Add(entity);
            db.SaveChanges();
        }

        public void InsertBulk(IEnumerable<T> list)
        {
            _entity.AddRange(list);
            db.SaveChanges();
        }

        public void Update(T entty)
        {
            _entity.Update(entty);
            db.SaveChanges();
        }

        int IRespository<T>.Insert(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
