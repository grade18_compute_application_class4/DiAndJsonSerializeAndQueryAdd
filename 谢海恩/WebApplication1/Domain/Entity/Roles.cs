﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Domain.Entity
{
    public class Roles:BaseEntity
    {
        public string RolesName { get; set; }

        public string Description { get; set; }

        public IEnumerable<Users> users { get; set; }
    }
}
