﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Domain.Entity;

namespace WebApplication1.Domain
{
    public class DbInitializer
    {
        public static void Seed()
        {
            using var db = new Admin3200DbContext();

            db.Database.EnsureCreated();

            var hasUser = db.Users.Any();

            if (!hasUser)
            {
                var roles = new Roles
                {
                    RolesName = "管理员",
                    Description = "没错，这就是一个猪猪管理员"
                };

                db.Roles.Add(roles);

                db.SaveChanges();

                db.Users.AddRange(new Users[]
                {
                    new Users
                    {
                        Username="",
                        Password="",
                        RolesId=roles.Id
                    },
                    new Users
                    {
                        Username="猪猪姗01号",
                        Password="123456",
                        RolesId=roles.Id
                    }
                });
                //保存更改
                db.SaveChanges();
            }
        }
    }
}
