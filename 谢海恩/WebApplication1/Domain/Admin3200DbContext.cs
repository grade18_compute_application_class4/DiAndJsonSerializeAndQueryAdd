﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Domain.Entity;

namespace WebApplication1.Domain
{
    public class Admin3200DbContext:DbContext
    {
        public Admin3200DbContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionStrig = "server=.;database=Admin3200;uid=sa;pwd=123456;";
            optionsBuilder.UseSqlServer(connectionStrig);
        }

        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }
    }
}
