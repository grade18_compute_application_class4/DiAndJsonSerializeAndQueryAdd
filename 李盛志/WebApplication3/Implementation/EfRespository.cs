﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication3.Domain;
using WebApplication3.Interface;

namespace WebApplication3.Implementation
{
    public class EfRespository<T> : IRespository<T> where T : BaseEntity
    {
        private readonly Admin200DbContext db;

        private DbSet<T> entity;
        public DbSet<T> Table
        {
            get
            {
                if(entity == null)
                {
                    entity = db.Set<T>();
                }
                return entity;
            }
        }
        public EfRespository(Admin200DbContext dbContext)
        {
            db = dbContext;
        }
        public void Delete(T entity)
        {
            this.Table.Remove(entity);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var row = Table.Where(x => x.Id == id).FirstOrDefault();
            Delete(row);
        }

        public T GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> GetTsAllEntity()
        {
            throw new NotImplementedException();
        }

        public int Insert(T entity)
        {
            throw new NotImplementedException();
        }

        public void InsertBulk(IEnumerable<T> list)
        {
            throw new NotImplementedException();
        }

        public void Update(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
