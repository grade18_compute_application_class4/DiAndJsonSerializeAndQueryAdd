﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication3.Domain.Entity;

namespace WebApplication3.Domain
{
    public class Admin200DbContext:DbContext
    {
        public Admin200DbContext()
        {
             
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = "server=.;database=Admin200;uid=sa;pwd=123456;";
            optionsBuilder.UseSqlServer(connectionString);
        }
        public DbSet<Users> Users { get; set; }
        public DbSet<Roles> Roles { get; set; }

    }
}
