﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Interface
{
    interface IRespository<T>
    {
        T GetById(int id);
        IEnumerable<T> GetTsAllEntity();

        int Insert(T entity);
        void InsertBulk(IEnumerable<T> list);
        void Update(T entity);
        void Delete(T entity);
        void Delete(int id);
    }
}
