﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiTest001.Domain.Entity;

namespace WebApiTest001.Domain
{
    public class DbInitializer
    {
        public static void Seed()
        {
            using (var db = new Admin4400DbContext())
            {
                //db.Database.EnsureCreated();
                db.Database.EnsureCreated();

                var hasUsers = db.Users.Any();

                if (!hasUsers)
                {
                    var role = new Roles
                    {
                        RoleName = "管理员",
                        Description = "这是一个管理员"
                    };

                    db.Roles.Add(role);

                    db.SaveChanges();

                    db.Users.AddRange(new Users[]
                    {
                        new Users
                        {
                            UserName = "admin",
                            PassWord = "admin",
                            RolesId = role.Id
                        },
                        new Users
                        {
                            UserName = "usr01",
                            PassWord = "123456",
                            RolesId = role.Id
                        }
                    });

                    db.SaveChanges();
                }
            }


        }
    }
}
