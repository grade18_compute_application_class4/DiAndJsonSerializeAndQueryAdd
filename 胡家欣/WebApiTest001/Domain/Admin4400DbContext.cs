﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiTest001.Domain.Entity;

namespace WebApiTest001.Domain
{
    public class Admin4400DbContext:DbContext
    {
        public Admin4400DbContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = "server=DESKTOP-M14GQ0V;database=Admin4400;uid=sa;pwd=123456;";
            optionsBuilder.UseSqlServer(connectionString);
        }

        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }
    }
}
