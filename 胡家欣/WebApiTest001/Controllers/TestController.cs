﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiTest001.Domain.Entity;
using WebApiTest001.Helper;
using WebApiTest001.Interface;

namespace WebApiTest001.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRespository<Users> _userRepository;

        public TestController(IRespository<Users> userRepository)
        {
            _userRepository = userRepository;
        }
        public string Get()
        {
            var list = _userRepository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}
