﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WebApiTest001.Domain;
using WebApiTest001.Domain.Entity;
using WebApiTest001.Helper;
//using Microsoft.Extensions.DependencyInjection;

namespace WebApiTest001.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly Admin4400DbContext _db;

        //[ActivatorUtilitiesConstructor]

        public WeatherForecastController(Admin4400DbContext dbContext)
        {
            _db = dbContext;
        }

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        //public WeatherForecastController(ILogger<WeatherForecastController> logger)
        //{
        //    _logger = logger;
        //}

        [HttpGet]
        public string Get()
        {
            //var rng = new Random();
            //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            //{
            //    Date = DateTime.Now.AddDays(index),
            //    TemperatureC = rng.Next(-20, 55),
            //    Summary = Summaries[rng.Next(Summaries.Length)]
            //})
            //.ToArray();

            var res = _db.Users.Include(x=>x.Roles).ToList();



            //return JsonConvert.SerializeObject(res,Formatting.Indented,new JsonSerializerSettings { 
            //    ReferenceLoopHandling=ReferenceLoopHandling.Ignore,
            //    DateFormatString = "yyyy-MM-dd HH:mm:ss"
            //});
            return JsonHelper.SerializeObject(res);
        }
    }
}
