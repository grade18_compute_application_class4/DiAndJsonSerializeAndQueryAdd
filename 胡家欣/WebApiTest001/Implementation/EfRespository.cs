﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiTest001.Domain;
using WebApiTest001.Domain.Entity;
using WebApiTest001.Interface;

namespace WebApiTest001.Implementation
{
    public class EfRespository<T> : IRespository<T> where T : BaseEntity
    {
        private readonly Admin4400DbContext db;

        private DbSet<T> _entity;

        protected DbSet<T> Entity 
        {
            get
            {
                if (_entity == null)
                {
                    _entity = db.Set<T>();
                }
                return _entity;
            }
        }

        public IQueryable<T> Table
        {
            get
            {
                
                return Entity;
            }
        }

        public EfRespository(Admin4400DbContext dbContext)
        {
            db = dbContext;
        }
        
        public void Delete(T entity)
        {
            this._entity.Remove(entity);

            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var row=Table.Where(x => x.Id == id).FirstOrDefault();
            Delete(row);
        }

        public T GetById(int id)
        {
            return _entity.Where(x => x.Id == id).FirstOrDefault();
        }

        public void Insert(T entity)
        {
            _entity.Add(entity);
            db.SaveChanges();
        }

        public void InsertBulk(IEnumerable<T> list)
        {
            _entity.AddRange(list);
            db.SaveChanges();
        }

        public void Update(T entity)
        {
            _entity.Update(entity);
            db.SaveChanges();
        }
    }
}
