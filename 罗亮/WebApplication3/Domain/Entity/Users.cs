﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Domain.Entity
{
    public class Users:BaseEntity
    {
       public string Username { get; set; }
        public string password { get; set; }
        
        public int RolesId { get; set; }
        public Roles Roles { get; set; }
    }
}
