﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Domain
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            IsActived = true;
            IsDelete = false;
            CreatedTime = DateTime.Now;
            UpdatedTime = DateTime.Now;
        }
        public int Id { get; set; }
        public bool IsActived { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public int DisplauOtder { get; set; }
        public string Remarks { get; set; }
    }
}
