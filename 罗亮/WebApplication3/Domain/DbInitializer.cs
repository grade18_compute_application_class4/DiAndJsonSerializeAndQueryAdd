﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication3.Domain.Entity;

namespace WebApplication3.Domain
{
    public class DbInitializer
    {
        public static void Seed()
        {
            using var db = new Admin200DbContext();
            db.Database.EnsureCreated();

            var hasUser = db.Users.Any();
            if (!hasUser)
            {
                var role = new Roles
                {
                    RoleName="你爹",
                    Description="没毛病，就是你爹"
                };
                db.Roles.Add(role);
                db.SaveChanges();
                db.Users.AddRange(new Users[] 
                {
                    new Users
                    {
                        Username="a",
                        password="1",
                        RolesId=role.Id
                    },
                    new Users
                    {
                        Username="b",
                        password="2",
                        RolesId=role.Id
                    },
                    new Users
                    {
                        Username="c",
                        password="3",
                        RolesId=role.Id
                    }

                });
                db.SaveChanges();
            }
        }
    }
}
