﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication3.Domain;
using WebApplication3.Domain.Entity;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using WebApplication3.Helper;

namespace WebApplication3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly Admin200DbContext _db;
 
        //public WeatherForecastController(Admin200DbContext dbContext)
        //{
        //    _db = dbContext; 
        //}


        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger,Admin200DbContext dbContext)
        {
            _logger = logger;
            _db = dbContext;
        }

        [HttpGet]
        public string Get()
        {
            //var rng = new Random();
            //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            //{
            //  Date = DateTime.Now.AddDays(index),
            //TemperatureC = rng.Next(-20, 55),
            //Summary = Summaries[rng.Next(Summaries.Length)]
            //})
            //.ToArray();
            var res = _db.Users.Include(x=>x.Roles).ToArray();

            return JosnHelper.SerializeObject(res);
        }
    } 
}
