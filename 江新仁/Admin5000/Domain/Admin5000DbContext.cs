﻿using Admin5000.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin5000.Domain
{
    public class Admin5000DbContext:DbContext
    {


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        {
            optionsBuilder.UseSqlServer("server=.;database=Admin5000;uid=sa;password=123456;");
        }

        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }
    }
}
