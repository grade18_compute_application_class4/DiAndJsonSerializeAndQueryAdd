﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin5000.Domain
{
    public class BaseEntity
    {

        public BaseEntity() 
        {
            IsActioned = true;
            IsDeleted = false;
            CreateTime = DateTime.Now;
            UpdateTime = DateTime.Now;
        }

        public int Id { get; set; }

        public bool IsActioned { get; set; }

        public bool IsDeleted { get; set; }

        public int DisplayOrder { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }
    }
}
