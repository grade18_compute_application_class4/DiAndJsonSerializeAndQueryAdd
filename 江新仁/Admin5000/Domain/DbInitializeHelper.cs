﻿using Admin5000.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin5000.Domain
{
    public class DbInitializeHelper
    {
        public static void Seed ()
        {
            using (var db=new Admin5000DbContext()) 
            {

                db.Database.EnsureCreated();
                var hsaUse = db.Users.Any();

                if (!hsaUse) 
                {
                    var role = new Roles
                    {
                        RoleName="管理员",
                        Description="这是这是一个管理员"
                    };

                    db.Roles.Add(role);
                    db.SaveChanges();

                    db.Users.AddRange(new Users[]
                    {
                        new Users
                        {
                            UserName="大壮",
                            Passsword="123456",
                            RolesId=role.Id
                        },
                        new Users
                        {
                            UserName="盖伦",
                            Passsword="123456",
                            RolesId=role.Id
                        }
                    });

                    db.SaveChanges();
                }
            }
        }
    }
}
