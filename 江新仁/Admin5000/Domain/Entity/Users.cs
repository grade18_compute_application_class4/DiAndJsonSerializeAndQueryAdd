﻿using Microsoft.AspNetCore.Authorization.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin5000.Domain.Entity
{
    public class Users:BaseEntity
    {
        public string UserName { get; set; }

        public string Passsword { get; set; }

        public int RolesId { get; set; }

        public Roles Roles { get; set; }
    }
}
