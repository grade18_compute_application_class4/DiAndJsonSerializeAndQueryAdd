﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin5000.Helper
{
    public class JsonHelper
    {
        public static string SerializeObject(object obj)
        {
            
            string str = JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings()
            {

                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatString = "yyyy-dd-mm hh:mm:ss"
            });
            return str;
        }
    }
}
