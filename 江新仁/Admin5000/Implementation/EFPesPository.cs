﻿using Admin5000.Domain;
using Admin5000.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin5000.Implementation
{
    public class EFPesPository<T> : IPespository<T> where T : BaseEntity
    {
        private readonly Admin5000DbContext db;


        private DbSet<T> _entity;


        protected DbSet<T> Entity
        {
            get
            {
                if (_entity == null)
                {
                    _entity = db.Set<T>();
                }
                return _entity;
            }
        }

        public IQueryable<T> Table
        {
            get
            {
                return Entity;
            }

        }
        // entity 和table 都是返回一个entity

        public EFPesPository(Admin5000DbContext dbcontext)
        {
            db = dbcontext;
        }

        public void Delete(T entity)
        {
            _entity.Remove(entity);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var row = Table.Where(x => x.Id == id).FirstOrDefault();
            Delete(row);
        }



        public T GetById(int id)
        {
            return Table.Where(x => x.Id == id).FirstOrDefault();

        }

        public void Insert(T entity)
        {
            Entity.Add(entity);
            db.SaveChanges();
        }

        public void InsertBulk(IEnumerable<T> list)
        {
            Entity.AddRange(list);
            db.SaveChanges();
        }

        public void Update(T entity)
        {
            Entity.Update(entity);
            db.SaveChanges();
        }
    }
}
