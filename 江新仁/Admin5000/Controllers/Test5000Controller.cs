﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin5000.Domain.Entity;
using Admin5000.Helper;
using Admin5000.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Admin5000.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Test5000Controller : ControllerBase
    {
        private readonly IPespository<Users> _userPespository;
        public Test5000Controller(IPespository<Users> userPespository) 
        {
            _userPespository = userPespository;
        }


        [Route("Get")]
        public string Get()
        {
            var list = _userPespository.Table.Include(x => x.Roles).ToList();
            return JsonHelper.SerializeObject(list);
        }

        [Route("GetId/{id}")]
      
        public string GetId(int id)
        {
            var list = _userPespository.GetById(id);

            return JsonHelper.SerializeObject(list);
        }

        [Route("DeleteId/{id}")]
        public string DeleteProduct(int id)
        {
            _userPespository.Delete(id);

            var list = _userPespository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
        [Route("insert")]
        public string Insert()
        {
            var user = new Users
            {
                UserName = "老胡",
                Passsword = "132654",
                RolesId = 3
            };
            _userPespository.Insert(user);
            return JsonHelper.SerializeObject(_userPespository.Table.ToList());
        }

        [Route("Update")]
        public string Update()
        {
            var user = new Users
            {
                Id = 5,
                UserName = "脑残片",
                Passsword = "7754321",
                RolesId = 3
            };
            _userPespository.Update(user);
            return JsonHelper.SerializeObject(_userPespository.Table.ToList());
        }
    }
}