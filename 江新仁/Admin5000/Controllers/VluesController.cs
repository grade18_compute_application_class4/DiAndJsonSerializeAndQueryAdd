﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin5000.Domain;
using Admin5000.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Admin5000.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VluesController : ControllerBase
    {

        private readonly Admin5000DbContext _db;

        public VluesController(Admin5000DbContext admin5000DbContext) 
        {
            _db = admin5000DbContext;
        }

        // GET: api/Test
        [HttpGet]
        public string Get()
        {
            var res = _db.Users.Include(x => x.Roles).ToList();
            return JsonHelper.SerializeObject(res);
        }

        // GET: api/Test/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Test
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Test/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
