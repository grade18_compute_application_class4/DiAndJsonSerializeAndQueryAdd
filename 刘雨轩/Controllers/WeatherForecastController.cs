﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JsonAndDependencyInjection.DoMain;
using JsonAndDependencyInjection.DoMain.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace JsonAndDependencyInjection.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly Admin4000Context _db;
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger,Admin4000Context db)
        {
            _logger = logger;
            _db = db;
        }

        [HttpGet]
        public string Get()
        {
            return JsonHelper.SerializeObject(_db.Users.Include(x => x.Role).ToList());
        }
    }
}
