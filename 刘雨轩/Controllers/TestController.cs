﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JsonAndDependencyInjection.DoMain.Entity;
using JsonAndDependencyInjection.DoMain.Helper;
using JsonAndDependencyInjection.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JsonAndDependencyInjection.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRespository<Users> _userRespository;
        public TestController(IRespository<Users> userRespository)
        {
            _userRespository = userRespository;
        }
        public string Get()
        {
            var list = _userRespository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}
