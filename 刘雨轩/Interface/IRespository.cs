﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JsonAndDependencyInjection.Interface
{
    public interface IRespository<T>
    {
        IQueryable<T> Table { get; }
        T GetById(int id);
        void Insert(T entity);
        void InsertBulk(IEnumerable<T> list);
        void Delete(T entity);
        void Delete(int id);
        void Update(T entity);
    }
}
