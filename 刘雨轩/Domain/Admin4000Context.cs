﻿using JsonAndDependencyInjection.DoMain.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JsonAndDependencyInjection.DoMain
{
    public class Admin4000Context:DbContext
    {
        public Admin4000Context() { }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=DESKTOP-NSGEMHH\\SQLEXPRESS;database=Admin4000;uid=sa;pwd=123456;");
        }
        public DbSet<Users> Users { get; set; }
        public DbSet<Roles> Roles { get; set; }
    }
}
