﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JsonAndDependencyInjection.DoMain
{
    public class DbInitializeHelper
    {
        public static void Initialize()
        {
            using (var db = new Admin4000Context())
            {
                db.Database.EnsureCreated();
                bool HasRole = db.Roles.Any();
                if (!HasRole)
                {
                    var role = new Entity.Roles
                    {
                        RoleName = "管理员",
                        Description = "这是一个管理员，管理个屁"
                    };
                    db.Roles.Add(role);
                    db.SaveChanges();
                    db.Users.AddRange(new Entity.Users[]
                    {
                    new Entity.Users
                    {
                        UserName = "admin",
                        PassWord = "admin",
                        RoleId = role.Id

                    },new Entity.Users
                    {
                        UserName = "sa",
                        PassWord = "123456",
                        RoleId = role.Id
                    }
                    });
                    db.SaveChanges();
                }
            };
            //using var db = new Admin4000Context();
            
        }
    }
}
