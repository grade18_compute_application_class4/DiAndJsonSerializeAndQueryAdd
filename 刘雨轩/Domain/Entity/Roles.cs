﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JsonAndDependencyInjection.DoMain.Entity
{
    public class Roles:BaseEntity
    {
        public string RoleName { get; set; }
        public string Description { get; set; }
        public IEnumerable<Users> User { get; set; }
    }
}
