﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JsonAndDependencyInjection.DoMain.Entity
{
    public class Users:BaseEntity
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public int RoleId { get; set; }
        public Roles Role { get; set; }
    }
}
