﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DIText001.DOMain.Entity;
using DIText001.Helper;
using DIText001.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DIText001.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TextController : ControllerBase
    {
        private readonly IRespository<Users> _userRepository;
        public TextController(IRespository<Users> userRepository) 
        {
            _userRepository = userRepository;
        }
        public string Get() 
        {
            var list = _userRepository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}