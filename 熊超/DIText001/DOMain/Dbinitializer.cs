﻿using DIText001.DOMain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIText001.DOMain
{
    public class Dbinitializer
    {
        public static void Seed()
        {
            using (var db = new Admin3000Context())
            {
                db.Database.EnsureCreated();
                var hasUser = db.Users.Any();
                if (!hasUser)
                {
                    var role1 = new Roles
                    {
                        RoleName = "熊超",
                        Description = "没错，我就是熊超"
                    };
                    db.Roles.Add(role1);
                    db.SaveChanges();
                    db.Users.AddRange(new Users[]
                    {
                        new Users
                        {
                            Username="xiongchao",
                            Password="123",
                            RoleId=role1.Id

                        },
                        new Users
                        {
                            Username="xiongchao1",
                            Password="12345",
                            RoleId=role1.Id
                        }
                    });
                }
                db.SaveChanges();
            }
        }
    }
}
