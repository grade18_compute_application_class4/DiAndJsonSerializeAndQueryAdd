﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiV.Domain.Enum
{
    public enum LogsLevel
    {
        信息=10,
        调试=20,
        警告=30,
        错误=40,
        崩溃=50,
    }
}
