﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebApiV.Domain
{
    public interface IRespository<T>
    {
        IQueryable<T> Table { get; }

        void Insert(T entity);
        void InsertBlck(IEnumerable<T> list);
        void Delete(int id);
        void Delete(T entity);
        void Delete(IEnumerable<T> entities);
        void Update(T entity);

        T GetById(int id);
    }
}
