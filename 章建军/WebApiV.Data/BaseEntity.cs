﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiV.Data
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            IsActived = true;
            IsDeleted = false;
            CreateTime = DateTime.Now;
            UpdateTime = DateTime.Now;
            Remarks = "what";
        }

        public int Id { get; set; }
        public bool IsActived { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public string Remarks { get; set; }
    }
}
