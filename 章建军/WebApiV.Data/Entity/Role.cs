﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiV.Data.Entity
{
    public class Role:BaseEntity
    {
        public string RoleName { get; set; }
        public string ShortTell { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}
