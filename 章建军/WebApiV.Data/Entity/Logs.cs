﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiV.Data.Entity
{
    public class Logs:BaseEntity
    {
        public int LogsLevel { get; set; }
        public string ShortMessage { get; set; }
        public string FullMessage { get; set; }
        public int UserId { get; set; }
        public string ReferenceUrl { get; set; }
    }
}
