﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiV.Data.Entity
{
    public class User:BaseEntity
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }
    }
}
