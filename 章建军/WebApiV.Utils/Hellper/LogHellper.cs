﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using WebApiV.Data.Entity;
using WebApiV.Domain.Enum;
using WebApiV.Interface.Log;

namespace WebApiV.Utils.Hellper
{
    public static class LogHellper
    {
        public static void Logger(this ILog log, LogsLevel logLevel, Exception exception, int userId)
        {
            var item = new Logs
            {
                LogsLevel = (int)logLevel,
                ShortMessage = exception.Message,
                FullMessage = exception.ToString(),
                UserId = userId
            };
            log.Insert(item);
        }

        public static void Info(this ILog log, Exception exception, LogsLevel logLevel = LogsLevel.信息, int userId = 0)
        {
            Logger(log, logLevel, exception, userId);
        }
        public static void Debug(this ILog log, Exception exception, LogsLevel logLevel = LogsLevel.调试, int userId = 0)
        {
            Logger(log, logLevel, exception, userId);
        }
        public static void Error(this ILog log, Exception exception, LogsLevel logLevel = LogsLevel.错误, int userId = 0)
        {
            Logger(log, logLevel, exception, userId);
        }
        public static void Fatal(this ILog log, Exception exception, LogsLevel logLevel = LogsLevel.崩溃, int userId = 0)
        {
            Logger(log, logLevel, exception, userId);
        }
    }
}
