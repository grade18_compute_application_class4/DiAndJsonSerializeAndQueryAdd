﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Text;
using WebApiV.Implement.Log;
using WebApiV.Interface.Log;
using WebApiV.Utils.Hellper;

namespace WebApiV.Utils.File
{
    public class ClobalExcptionFilter : IExceptionFilter
    {
        private readonly ILog _log;

        public ClobalExcptionFilter(ILog log)
        {
            _log = log;
        }

        public void OnException(ExceptionContext context)
        {
            _log.Error(context.Exception);
            context.ExceptionHandled = true;
        }
    }
}
