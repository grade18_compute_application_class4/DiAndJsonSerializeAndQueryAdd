﻿using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using WebApiV.Data;
using WebApiV.Data.Entity;
using WebApiV.Domain;

namespace WebApiV.Utils
{
    public class DbInitlizer
    {
        public static void Seed(IServiceProvider service)
        {
            using(var score = service.CreateScope())
            {
                var db = score.ServiceProvider.GetService(typeof(Admin10086Db))as Admin10086Db;
                var _user = score.ServiceProvider.GetService(typeof(IRespository<User>))as IRespository<User>;
                var _role = score.ServiceProvider.GetService(typeof(IRespository<Role>)) as IRespository<Role>;
                db.Database.EnsureCreated();

                var emp = db.Users.Any();
                if (!emp)
                {
                    var rloe1 = new Role
                    {
                        RoleName = "老王隔壁",
                        ShortTell = "就是老黄",
                    };
                    _role.Insert(rloe1);

                    _user.InsertBlck(new User[]
                    {
                    new User
                    {
                        UserName="sa",
                        PassWord="123",
                        RoleId=rloe1.Id,
                    },
                    new User
                    {
                        UserName="sb",
                        PassWord="456",
                        RoleId=rloe1.Id,
                    }
                    });
                }               
            }
        }
    }
}
