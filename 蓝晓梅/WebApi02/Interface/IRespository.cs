﻿using Microsoft.EntityFrameworkCore.Migrations.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi02.Interface
{
    public interface IRespository<T>
    {
        IQueryable<T> Table { get; }

        T GetById(int id);
        //IEnumerable<T> GetAllEntity();

        void Insert(T entity);
        void InsertBulk(IEnumerable<T> list);
        void Update(T entity);
        void Delete(T entity);
        //重载
        void Delete(int id);
        

    }
}
