﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi02.Domain;
using WebApi02.Domain.Entity;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using WebApi02.Helper;

namespace WebApi02.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly Admin02Dbcontent _db;

        //第二种方式：这个版本不支持两个及两个以上的构造函数
        //[ActivatorUtilitiesConstructor]

        //第一个方式：自己加的构造函数
        public WeatherForecastController(Admin02Dbcontent dbContext)
        {
            _db = dbContext;
        }

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        //第二种方式：这个版本不支持两个及两个以上的构造函数
        //public WeatherForecastController(ILogger<WeatherForecastController> logger)
        //{
        //    _logger = logger;
        //}

        //第三种方式：不要自己加的构造函数，在后面原来的基础上加Admin02Dbcontent dbcontent
        //public WeatherForecastController(ILogger<WeatherForecastController> logger,Admin02Dbcontent dbcontent)
        //{
        //    _logger = logger;
        //    _db = dbcontent;
        //}

        [HttpGet]
        public string Get()
        {
            var res= _db.Students.ToList();



            //json数据格式化，依赖注入,忽略这个循环引用
            return JsonHelper.SerializeObject(res);
        }
    }
}
