﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi02.Domain.Entity;
using WebApi02.Helper;
using WebApi02.Interface;

namespace WebApi02.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRespository<Students> _studentRepository;
       
        public TestController(IRespository<Students> studentRepository)
        {
            _studentRepository = studentRepository;
        }

        public string Get()
        {
            var list = _studentRepository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}
