﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi02.Domain.Entity;

namespace WebApi02.Domain
{
    public class DbInitializer
    {
        //种子数据
        public static void Seed()
        {
            using var db = new Admin02Dbcontent();
            db.Database.EnsureCreated();

            var hasStudent = db.Students.Any();

            if (!hasStudent)
            {
                var grade = new Grades
                {
                    GradeName = "计应01班",
                    Description = "计算机应用技术1班"
                };

                db.Grades.Add(grade);
                
                db.SaveChanges();

                //用户类型的数组
                db.Students.AddRange(new Students[]
                {
                    new Students
                    {
                        Studentname="张三",
                        Studentcode="001",
                        GradeId=grade.Id
                    },
                    new Students
                    {
                        Studentname="李四",
                        Studentcode="002",
                        GradeId=grade.Id
                    }
                });

                db.SaveChanges();
            }
        }
    }
}
