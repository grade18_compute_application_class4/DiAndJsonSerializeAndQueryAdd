﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi02.Domain
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            IsActived = true;
            IsDeleted = false;
            Createdtime = DateTime.Now;
            Updatedtime = DateTime.Now;
        }
        public int Id { get; set; }

        public bool IsActived { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime Createdtime { get; set; }
        public DateTime Updatedtime { get; set; }
    }
}
