﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi02.Domain.Entity;

namespace WebApi02.Domain
{
    public class Admin02Dbcontent:DbContext
    {
        public DbSet<Students> Students { get; set; }
        public DbSet<Grades> Grades { get; set; }

        //构造函数
        public Admin02Dbcontent()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = "server=.;database=Admin02;uid=sa;pwd=A123456;";
            optionsBuilder.UseSqlServer(connectionString);

        }
    }
}
