﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi02.Domain.Entity
{
    public class Students:BaseEntity
    {
        public string Studentname { get; set; }
        public string Studentcode { get; set; }

        //外键
        public int GradeId { get; set; }

        public Grades Grades { get; set; }
    }
}
