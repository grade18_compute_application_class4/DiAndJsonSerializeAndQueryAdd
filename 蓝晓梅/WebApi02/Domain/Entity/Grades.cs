﻿using System.Collections;
using System.Collections.Generic;

namespace WebApi02.Domain.Entity
{
    public class Grades:BaseEntity
    {
        public string GradeName { get; set; }
        public string Description { get; set; }

        public IEnumerable<Students> Students { get; set; }

    }
}