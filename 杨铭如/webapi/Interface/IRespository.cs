﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Interface
{
    public interface IRespository<T>
    {
        IQueryable<T> Table { get; }
        T GetById(int id);

        void Insert(T entity);
        void InserBulk(IEnumerable<T>list);
        void Update(T entity);
        void Delete(T entity);
        void Delete(int id);
    }
}
