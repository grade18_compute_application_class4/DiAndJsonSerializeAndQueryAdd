﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using webapi.Domin.Entity;
using webapi.Helper;
using webapi.Interface;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DominController : ControllerBase
    {
        private readonly IRespository<Users> _userRespository;

        public DominController(IRespository<Users> userRespository)
        {
            _userRespository = userRespository;
        }

        public string Get() 
        {
            var list = _userRespository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}