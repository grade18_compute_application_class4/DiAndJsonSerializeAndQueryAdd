﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Domin.Entity
{
    public class Users:BaseEntity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int RolesId { get; set; }
        public Roles Roles { get; set; }
    }
}
