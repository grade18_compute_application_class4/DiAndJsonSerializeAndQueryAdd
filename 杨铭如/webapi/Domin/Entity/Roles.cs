﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Domin.Entity
{
    public class Roles:BaseEntity
    {
        public string RolesName { get; set; }
        public string Description { get; set; }
        public IEnumerable<Users> Users { get; set; }
    }
}
