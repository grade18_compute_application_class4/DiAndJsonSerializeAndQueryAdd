﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi.Domin.Entity;

namespace webapi.Domin
{
    public class Admin977Context:DbContext
    {
        public Admin977Context()
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var con = "server=.;database=Admin977;uid=sa;pwd=123456";
            optionsBuilder.UseSqlServer(con);
        }

        public DbSet<Users> Users { get; set; }
        public DbSet<Roles> Roles { get; set; }
    }
}
