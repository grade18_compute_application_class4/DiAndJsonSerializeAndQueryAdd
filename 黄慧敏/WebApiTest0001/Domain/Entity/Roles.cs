﻿using System.Collections.Generic;

namespace WebApiTest0001.Domain.Entity
{
    public class Roles:BaseEntity
    {
        public string RoleName { get; set; }

        public string Description { get; set; }

        public IEnumerable<Users> Users { get; set; }
    }
}