﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiTest0001.Domain.Entity
{
    public class Users: BaseEntity
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public int RolesId { get; set; }

        public Roles Roles { get; set; }
    }
}
