﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiTest0001.Helper
{
    public class JsonHelper
    {
        public static string SerializeObject(object obj)
        {
          
            return JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatString = "yyy-MM-dd HH:mm:ss"
            });
        }
    }
}
