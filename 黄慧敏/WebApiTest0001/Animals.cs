﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiTest0001
{
    public class Animals
    {
        public virtual void Eat()
        {
            Console.WriteLine("动物进食");
        }
    }

    public class Dog : Animals
    {
        public override void Eat() //重写
        {
            Console.WriteLine("小狗汪汪，吃骨头");
        }
    }

    public class Cat : Animals
    {
        public override void Eat()
        {
            Console.WriteLine("小猫喵喵，抓老鼠");
        }
    }
}
