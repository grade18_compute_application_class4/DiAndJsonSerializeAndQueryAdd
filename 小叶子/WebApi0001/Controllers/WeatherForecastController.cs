﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi0001.Domain.Model;
using WebApplication1.Domain;
using Microsoft.Extensions.DependencyInjection;
using WebApi0001.Helper;
using Microsoft.EntityFrameworkCore;

namespace WebApi0001.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {

        //依赖注入方法 1
        private readonly Admin1000DbContext _db;

        [ActivatorUtilitiesConstructor]
        public WeatherForecastController(Admin1000DbContext dbContext)
        {
            _db = dbContext;
        }


        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        //方法2：注释logger
        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }
       

        //方法3
        //public WeatherForecastController(ILogger<WeatherForecastController> logger，Admin5000DbContext _db)
        //{
        //    _logger = logger;
        //    _db = dbContext;
        //}
        [HttpGet]
        public string Get()
        {
            var res = _db.Students.Include(x => x.Major).ToList();
            return JsonHelper.SerializeObject(res);
        }



        //public IEnumerable<Student> Get()
        //{
        //    var list = new List<Student>();
        //    using (var db = new Admin1000DbContext())
        //    {
        //        list = db.Students.ToList<Student>();
        //    }
        //    return list;

        //}
    }
}
