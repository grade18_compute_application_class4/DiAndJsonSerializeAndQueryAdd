﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using WebApi0001.Domain.Model;
using WebApi0001.Helper;
using WebApi0001.Interface;

namespace WebApi0001.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {

        private readonly IRespository<Student> _userReapositoty;

        public TestController(IRespository<Student> userRespository)
        {
            _userReapositoty = userRespository;
        }

        public string Get()
        {
            var list = _userReapositoty.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}
