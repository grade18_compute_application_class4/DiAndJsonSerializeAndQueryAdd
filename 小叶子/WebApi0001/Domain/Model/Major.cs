﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi0001.Controllers;

namespace WebApi0001.Domain.Model
{
    public class Major : BaseEntity
    {
        public string MajorName { get; set; }

        public string MajorDescription { get; set; }
        
        public IEnumerable<Student> Students { get; set; } 
    }
}
