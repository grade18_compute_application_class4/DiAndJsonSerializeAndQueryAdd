﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi0001.Controllers;

namespace WebApi0001.Domain.Model
{
    public class Student : BaseEntity
    {
        public string StudentName { get; set; }

        public string StudentSex { get; set; }

        public int StudentAge { get; set; }
              

        public int MajorId { get; set; }

        public Major Major { get; set; }
    }
}
