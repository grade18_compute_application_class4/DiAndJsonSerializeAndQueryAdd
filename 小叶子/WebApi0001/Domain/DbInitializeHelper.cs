﻿using System.Linq;
using System.Security.Claims;
using WebApi0001.Domain.Model;

namespace WebApplication1.Domain
{
    public class DbInitializeHelper
    {
        public static void Initilizer()
        {

            using var db = new Admin1000DbContext();
            db.Database.EnsureCreated();

            var hasStudents = db.Students.Any();
            if (!hasStudents)
                {
                    var Majors = new Major
                    {
                        MajorName = "四年级二班",
                        MajorDescription = "一个品牌"
                    };
                    db.Majors.Add(Majors);

                    db.SaveChanges();

                    db.Students.AddRange(new Student[]
                    {
                    new Student
                    {
                        StudentName="张xx",
                        StudentSex="男",
                        StudentAge=18,
                        MajorId=Majors.id
                    },
                    new Student
                    {
                       StudentName="李xx",
                        StudentSex="女",
                        StudentAge=18,
                        MajorId=Majors.id
                    }
                    });

                    db.SaveChanges();
                }
            }


        }
    }

