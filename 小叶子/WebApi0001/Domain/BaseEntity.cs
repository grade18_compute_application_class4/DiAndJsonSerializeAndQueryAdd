﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi0001.Controllers
{
    public abstract class BaseEntity
    {
        public BaseEntity()
        {
            IsActived = true;
            IsDeleted = false;
            CreateTime = DateTime.Now;
            UpdateTime = DateTime.Now;
        }

        public int id { get; set; }

        public bool IsActived { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public string Remarks { get; set; }
    }
}
