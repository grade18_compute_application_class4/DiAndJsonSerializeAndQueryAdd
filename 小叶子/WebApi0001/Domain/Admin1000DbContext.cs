﻿using Microsoft.EntityFrameworkCore;
using WebApi0001.Domain.Model;

namespace WebApplication1.Domain
{
    public class Admin1000DbContext : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=.;database=Admin1000;uid=sa;pwd=123456;");
        }

        public DbSet<Major> Majors { get; set; }

        public DbSet<Student> Students { get; set; }
    }
}
