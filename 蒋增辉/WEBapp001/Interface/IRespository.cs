﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEBapp001.Domain;

namespace WEBapp001.Interface
{
    public interface IRespository<T>
    {
        IQueryable<T> Table { get; }

        T GitById(int id);
        

        void Insert(T entity);

        void BulkInsert(IEnumerable<T> list);

        void Update(T entity);

        void Delete(T entity);

        void Delete(int id);
    }
}
