﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEBapp001.Domain.Entity;

namespace WEBapp001.Domain
{
    public class DbInitializer
    {
        public static void Seed()
        {
            using (var db=new DbSchoolContext())
            {
                db.Database.EnsureCreated();

                var IsEsixt = db.Students.Any();
                if (!IsEsixt)
                {

                    var Collge = new Collge
                    {
                        CollgeName="忍术科",
                        DeanName="猿飞日斩"

                    };

                    db.Collges.Add(Collge);
                    db.SaveChanges();

                    db.Students.AddRange(new Student[] { 
                        new Student
                        {
                            StuName="宇智波鼬",
                            StuAge=26,
                            StuPri="火之国木叶忍村",
                            StuSex="男",
                            Remarske="宇智波鼬，火之国木叶隐村宇智波一族的天才忍者。年幼时他与宇智波止水是挚友，实力强大，擅长使用幻术"

                        },
                        new Student
                        {
                            StuName="宇智佐助",
                            StuAge=17,
                            StuPri="火之国木叶忍村",
                            StuSex="男",
                            Remarske="佐助是一个具有强大意志的人。在忍者学校，才貌双全的佐助很受女生欢迎，但他丝毫未受影响，为自己的目标不断提高能力。后来他以第一名的成绩在忍者学校毕业，是全村最为期待的头号优等生。“我可是拼了命去做的，不要用‘天才’两个字抹杀掉我的努力！”这是佐助对自己的诠释"
                        },
                        new Student
                        {
                            StuName="宇智止水",
                            StuAge=29,
                            StuPri="火之国木叶忍村",
                            StuSex="男",
                            Remarske="宇智波止水，火之国木叶隐村宇智波一族的天才忍者，年幼时他与宇智波鼬是挚友，有着比鼬更强的瞳术天赋。实力强大，擅长使用幻术和瞬身术，以“瞬身止水”之名威震天下。他身为宇智波镜的后代，不仅是宇智波鼬的导师，同时也是宇智波鼬的挚友，宇智波鼬对其如亲生兄长一般尊敬。"
                        }
                    });
                    db.SaveChanges();

                   
                    


                }
            }
        }
    }
}
