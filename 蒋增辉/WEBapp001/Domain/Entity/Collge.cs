﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBapp001.Domain.Entity
{
    public class Collge:BaseEntity
    {
        public string CollgeName { get; set; }
        public string DeanName { get; set; }

        public DbSet<Student> Studentid { set; get; }
    }
}
