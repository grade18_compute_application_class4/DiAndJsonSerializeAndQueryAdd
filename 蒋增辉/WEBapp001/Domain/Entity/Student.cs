﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEBapp001.Domain.Entity;

namespace WEBapp001.Domain
{
    public class Student:BaseEntity
    {
        public string StuName { get; set; }

        public string StuSex { get; set; }

        public string StuPri { get; set; }

        public int StuAge { get; set; }

     
        
     
    }
}
