﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEBapp001.Domain
{
    public abstract class BaseEntity
    {
        public BaseEntity()
        {
            IsActived = true;
            IsDeleted = false;
            CreatedTime = DateTime.Now;
            UpDatedTime = DateTime.Now;
            
        }

        public int Id { set; get; }

        public DateTime CreatedTime { get; set; }

        public DateTime UpDatedTime { get; set; }

        public bool IsActived { get; set; }

        public bool IsDeleted { get; set; }

        public string Remarske { get; set; }

    }
}
