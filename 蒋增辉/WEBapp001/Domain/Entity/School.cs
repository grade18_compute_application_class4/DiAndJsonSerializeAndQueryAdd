﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEBapp001.Domain.Entity;

namespace WEBapp001.Domain
{
    public class School:BaseEntity
    {
        public string SchoolName { get; set; }

        public string SchoolPriv { get; set; }

        public string Schoolcolle { get; set; }

        public DbSet<Collge> Collges { get; set; }

    }
}
