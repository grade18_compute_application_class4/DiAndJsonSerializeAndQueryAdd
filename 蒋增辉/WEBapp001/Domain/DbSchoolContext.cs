﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEBapp001.Domain.Entity;

namespace WEBapp001.Domain
{
    public class DbSchoolContext :DbContext
    {
        public DbSchoolContext()
        {

        }
       protected override   void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=.;database=School;uid=sa;pwd=123456");
        }

        public DbSet<Student> Students { get; set; }

        public DbSet<School> Schools { get; set; }

        public DbSet<Collge> Collges { get; set; }
    }
}
