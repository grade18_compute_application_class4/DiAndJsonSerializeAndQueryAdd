﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json.Serialization;
using WEBapp001.Domain;
using WEBapp001.Helper;
using WEBapp001.Interface;
using JsonHelper = WEBapp001.Helper.JsonHelper;

namespace WEBapp001.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRespository<Student> Studentrespository;
        public TestController(IRespository<Student> respository)
        {
            Studentrespository = respository;
        }

        public string  Get()
        {
            var res = Studentrespository.Table.ToList();
           return JsonHelper.SerializeObject(res);
        }

    }
}