﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WEBapp001.Domain;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System.CodeDom.Compiler;
using WEBapp001.Helper;

namespace WEBapp001.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        private readonly DbSchoolContext _db;

        [ActivatorUtilitiesConstructor]
        public ValuesController(DbSchoolContext dbSchoolContext)
        {
            _db = dbSchoolContext;
        }
        // GET api/values
        [HttpGet]
        public string Get()
        {
           var res= _db.Students.ToList();
            return JsonHelper.SerializeObject(res);
            
        }

        // GET api/values/5
      
     

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
