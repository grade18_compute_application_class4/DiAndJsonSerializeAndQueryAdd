﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using DIText001.DOMain;
using DIText001.DOMain.Entity;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using DIText001.Helper;

namespace DIText001.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly Admin3000Context _db;
        //[ActivatorUtilitiesConstructor]
        //public WeatherForecastController(Admin3000Context dbContext) 
        //{
        //    _db = dbContext;
        //}
       
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger,Admin3000Context dbContext)
        {
            _logger = logger;
            _db = dbContext;
        }

        [HttpGet]
        public string Get()
        {
            //var rng = new Random();
            //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            //{
            //    Date = DateTime.Now.AddDays(index),
            //    TemperatureC = rng.Next(-20, 55),
            //    Summary = Summaries[rng.Next(Summaries.Length)]
            //})
            //.ToArray();
            var res = _db.Users.Include(x=>x.Roles).ToList();

            return JsonHelper.SerializeObject(res);
        }
    }
}
