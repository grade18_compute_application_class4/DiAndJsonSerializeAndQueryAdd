﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using WebAPI__DI_json.DoMain.Entity;

namespace WebAPI__DI_json.DoMain
{
    public class AdminDbContext:DbContext
    {
        public AdminDbContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=N8QU1VYXG0M3H5J;Database=Admin;uid=sa;pwd=123456");
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Role> Roles { get; set; }
    }
}
