﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using WebAPI__DI_json.DoMain.Entity;
using WebAPI__DI_json.Implementation;
using WebAPI__DI_json.Interface;

namespace WebAPI__DI_json.DoMain
{
    public class DbInitializer
    {
        public static void Seed(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                var db = scope.ServiceProvider.GetService(typeof(AdminDbContext)) as AdminDbContext;//强制轉換類型
                var _userRespository = scope.ServiceProvider.GetService(typeof(IRespository<User>)) as EfRespository<User>;
                var _RoleRespository = scope.ServiceProvider.GetService(typeof(IRespository<Role>)) as EfRespository<Role>;

                db.Database.EnsureCreated();

                var HasUser = db.Users.Any();
                if (!HasUser)
                {
                    var role = new Role
                    {
                        RoleName = "管理员",
                        Description = "此处省略一百字"
                    };

                    _RoleRespository.Insert(role);

                    _userRespository.InsertBulk(new User[]
                    {
                        new User
                        {
                            UserName ="Admin",
                            PassWord="123456",
                            RoleId = role.Id
                        }
                        ,
                        new User
                        {
                            UserName ="User01",
                            PassWord="654321",
                            RoleId = role.Id
                        }
                    });




                }
            }

            //var db = new AdminDbContext();

        }
    }
}
