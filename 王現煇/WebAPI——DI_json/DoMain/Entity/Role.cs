﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI__DI_json.DoMain.Entity
{
    public class Role:BaseEntity
    {
        public string RoleName { get; set; }

        public string Description { get; set; }

        public IEnumerable<User> Users { get; set; }
    }
}
