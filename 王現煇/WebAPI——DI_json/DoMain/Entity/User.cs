﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI__DI_json.DoMain.Entity
{
    public class User:BaseEntity
    {
        public string UserName { get; set; }

        public string PassWord { get; set; }

        public int RoleId { get; set; }

        public Role Role { get; set; }
    }
}
