﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Diagnostics;
using WebAPI__DI_json.DoMain.Entity;
using WebAPI__DI_json.Helper;
using WebAPI__DI_json.Interface;

namespace WebAPI__DI_json.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRespository<User> _UserRespository;
        private readonly IRespository<Role> _RoleRespository;

        public TestController(IRespository<User> Urespository, IRespository<Role> Rrespository)
        {
            _UserRespository = Urespository;
            _RoleRespository = Rrespository;
        }

        public string Get()
        {
            var users = _UserRespository.Table.ToList();

            //var roles = _RoleRespository.Table.FirstOrDefault();

            //_UserRespository.Insert(new User
            //{
            //    UserName = "www",
            //    PassWord = ".com",
            //    RoleId = roles.Ide
            //});

            return JsonHelper.JsonSerializeSetting(users);
        }
    }
}
