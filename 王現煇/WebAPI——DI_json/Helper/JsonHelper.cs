﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI__DI_json.Helper
{
    public class JsonHelper
    {
        public  static string JsonSerializeSetting(object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ContractResolver = new DefaultContractResolver(),
                DateFormatString = "yyyy-MM-dd HH:mm:ss"
            });
        }
    }
}
