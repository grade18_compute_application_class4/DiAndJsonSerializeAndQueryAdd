﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI__DI_json.Interface
{
    public interface IRespository<T>
    {
        T GetById(int Id);

        IQueryable<T> Table { get; }

        void Insert(T entity);

        void InsertBulk(IEnumerable<T> list);

        void Update(T entity);

        void Delete(T entity);

        void Delete(int Id);

    }
}
