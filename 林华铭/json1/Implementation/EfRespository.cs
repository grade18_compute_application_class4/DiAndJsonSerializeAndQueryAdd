﻿using json1.Domain;
using json1.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace json1.Implementation
{
    public class EfRespository<T> : Irespository<T> where T : BaseEntity
    {
        private readonly Admin2000DbContext db;

        private DbSet<T> _entity;

        private DbSet<T> Entity {
            get{
                if (_entity == null)
                {
                    _entity = db.Set<T>();
                }
                return _entity;
            }
        }

        public IQueryable<T> Table
        {
            get
            {
                return Entity;
            }
        }

        public EfRespository(Admin2000DbContext dbContext)
        {
            db = dbContext;
        }

        public void Delete(T entity)
        {
            this._entity.Remove(entity);

            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var row = Table.Where(x => x.id == id).FirstOrDefault();
            Delete(row);
        }

        public T GetById(int id)
        {
            return _entity.Where(x => x.id == id).FirstOrDefault();
        }

        public void Insert(T entity)
        {
            _entity.Add(entity);
            db.SaveChanges();
        }

        public void InsertBulk(IEnumerable<T> list)
        {
            _entity.AddRange(list);
            db.SaveChanges();
        }

        public void Update(T entity)
        {
            _entity.Update(entity);
            db.SaveChanges();
        }
    }
}
