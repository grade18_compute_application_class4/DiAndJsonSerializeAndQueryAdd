﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using json1.DbHelper;
using json1.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace json1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        //依赖
        private readonly Admin2000DbContext _db;

        public ValuesController(Admin2000DbContext dbContext)
        {
            _db = dbContext;
        }

        // GET api/values
        [HttpGet]
        //public string Get()
        //{
        //    var list = new List<Staffs>();
        //    using (var db = new Admin2000DbContext())
        //    {
        //        list = db.Staffs.ToList<Staffs>();
        //    } 
        //    string str = JsonConvert.SerializeObject(list,Formatting.Indented);
        //    var list1 = new List<studio>();
        //    using (var db = new Admin2000DbContext())
        //    {
        //        list1 = db.Studios.ToList<studio>();
        //    }
        //    string str1 = JsonConvert.SerializeObject(list1, Formatting.Indented);
        //    var list2 = new List<work>();
        //    using (var db = new Admin2000DbContext())
        //    {
        //        list2 = db.Works.ToList<work>();
        //    }
        //    string str2 = JsonConvert.SerializeObject(list2, Formatting.Indented);
        //    return str+str1+str2;
        //}
        public string Get()
        {
            var res = _db.Staffs.Include(x => x.Studios).ToList();

            return JsonHelper.SerializeObject(res);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
