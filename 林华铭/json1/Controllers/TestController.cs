﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using json1.DbHelper;
using json1.Domain;
using json1.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace json1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly Irespository<Staffs> _staffrepository;

        public TestController(Irespository<Staffs> staffirespository)
        {
            _staffrepository = staffirespository;
        }

        public string Get()
        {
            var list = _staffrepository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }

        //GetById根据ID查找数据返回结果(不会用)

        //public string Get(int id)
        //{
        //    var list = _staffrepository.GetHashCode();
        //    return JsonHelper.SerializeObject(list);
        //}

        //GetHashCode获取哈希值

        //public string Get()
        //{
        //    var list = _staffrepository.GetHashCode();
        //    return JsonHelper.SerializeObject(list);
        //}

        //GetType获取对象type类型

        //public string Get()
        //{
        //    var list = _staffrepository.GetType();
        //    return JsonHelper.SerializeObject(list);
        //}
    }
}