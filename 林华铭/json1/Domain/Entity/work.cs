﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace json1.Domain
{
    public class work : BaseEntity
    {
        public string workName { get; set; }

        public int staffId { get; set; }

        public Staffs Staff { get; set; }
    }
}
