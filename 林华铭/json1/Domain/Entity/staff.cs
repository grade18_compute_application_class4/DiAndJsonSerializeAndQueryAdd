﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace json1.Domain
{
    public class Staffs:BaseEntity
    {
        public string staffName { get; set; }

        public string Description { get; set; }

        public IEnumerable<studio> Studios { get; set; }

        public IEnumerable<work> Works { get; set; }

        //public virtual int Add(int a,int b)
        //{
        //    return a + b;
        //}
    }
    //public abstract class AA
    //{
    //    public abstract int Add(double a, double b);
    //}
}
