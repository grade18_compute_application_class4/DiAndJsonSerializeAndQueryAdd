﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace json1.Domain
{
    public class DbInitializer
    {
        public static void Seed()
        {
            using (var db =new Admin2000DbContext())
            {
                db.Database.EnsureCreated();

                    var astudio = db.Studios.Any();

                    if (!astudio)
                    {
                        var staff = new Staffs
                        {
                            staffName = "员工",
                            Description = "就是员工啊"
                        };
                        var staff1 = new Staffs
                        {
                            staffName = "员工",
                            Description = "就是员工啊"
                        };

                    db.Staffs.Add(staff);

                    db.Staffs.Add(staff1);

                    db.SaveChanges();

                    //db.Studios.Add(new studio
                    //{
                    //    studioName = "工作室",
                    //    staffId = staff.id
                    //});
                    //db.SaveChanges();

                    db.Studios.AddRange(new studio[]
                    {
                        new studio
                        {
                            studioName = "工作室",
                            staffId = staff.id
                        },
                        new studio
                        {
                            studioName = "工作室1",
                            staffId = staff.id
                        },
                        new studio
                        {
                            studioName = "工作室2",
                            staffId = staff.id
                        }
                    });
                    db.SaveChanges();


                    //db.Works.Add(new work
                    //    {
                    //        workName = "工作",
                    //        staffId = staff.id
                    //    });
                    //    db.SaveChanges();

                    db.Works.AddRange(new work[]
                    {
                        new work
                        {
                            workName = "工作",
                            staffId = staff.id
                        },
                        new work
                        {
                            workName = "工作1",
                            staffId = staff.id
                        },
                        new work
                        {
                            workName = "工作2",
                            staffId = staff.id
                        }
                    });
                    db.SaveChanges();
                }
            }
        }
    }
}
