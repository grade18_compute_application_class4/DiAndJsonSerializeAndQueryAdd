﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace json1.Domain
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            IsActived = true;
            IsDeleted = false;
            CreatedTime = DateTime.Now;
            UpdatedTime = DateTime.Now;
        }

        public int id { get; set; }

        public bool IsActived { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime UpdatedTime { get; set; }

        public string Remarks { get; set; }
    }
}
