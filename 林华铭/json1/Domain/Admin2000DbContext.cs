﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace json1.Domain
{
    public class Admin2000DbContext:DbContext
    {
        public Admin2000DbContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = "server=.;database=Admin2000;uid=sa;pwd=123456";
            optionsBuilder.UseSqlServer(connectionString);
        }

        public DbSet<Staffs> Staffs { get; set; }

        public DbSet<studio> Studios { get; set; }

        public DbSet<work> Works { get; set; }
    }
}
