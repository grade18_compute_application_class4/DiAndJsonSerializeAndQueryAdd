﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi0001.Domain.Entity;
using WebApi0001.Helper;
using WebApi0001.implementation;

namespace WebApi0001.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRespository<Users> _userRepository;

        public TestController(IRespository<Users> userRepository)
        {
            _userRepository = userRepository;
        }
        public string Get()
        {
            var list = _userRepository.table.ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}