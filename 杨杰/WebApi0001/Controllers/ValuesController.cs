﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi0001.Domain;
using WebApi0001.Domain.Entity;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using WebApi0001.Helper;

namespace WebApi0001.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly Admin12300DbContext _db;

        [ActivatorUtilitiesConstructor]

        public ValuesController(Admin12300DbContext dbContext)
        {
            _db = dbContext;
        }
        // GET api/values
        [HttpGet]
        public string Get()
        {
            var res = _db.Users.Include(x => x.Roles).ToList();

            return JsonHelper.SerializeObject(res);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
