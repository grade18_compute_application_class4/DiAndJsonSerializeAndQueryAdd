﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi0001.Domain.Entity
{
    public class Users:BaseEntity
    {
        public string UserName { get; set; }

        public string UserPwd { get; set; }

        public int RoleId { get; set; }

        public Roles Roles { get; set; }
    }
}
