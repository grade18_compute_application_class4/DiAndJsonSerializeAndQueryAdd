﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi0001.Domain.Entity
{
    public class Roles : BaseEntity
    {
        public string RoleName { get; set; }

        public string Description { get; set; }

        public IEnumerable<Users> Users { get; set; }
    }
}
