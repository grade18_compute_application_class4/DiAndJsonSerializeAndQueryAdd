﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi0001.Domain.Entity;

namespace WebApi0001.Domain
{
    public class DbInitializer
    {
        public static void Seed()
        {
            var db = new Admin12300DbContext();
            db.Database.EnsureCreated();

            var qwq = db.Users.Any();
            
            if (!qwq)
            {
                var role = new Roles
                {
                    RoleName = "不祥之刃",
                    Description = "你将会是一道美丽的风景线",
                };
                db.Roles.Add(role);

                db.SaveChanges();

                db.Users.AddRange(new Users[] {
                    new Users
                    {
                        UserName = "艾欧尼亚",
                        UserPwd = "123456",
                        RoleId = role.Id
                    },
                    new Users
                    {
                        UserName = "昂扬不灭",
                        UserPwd = "456789",
                        RoleId = role.Id
                    }
                });        
                db.SaveChanges();
            }
        }
    }
}
