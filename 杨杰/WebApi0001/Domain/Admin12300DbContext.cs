﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi0001.Domain.Entity;

namespace WebApi0001.Domain
{
    public class Admin12300DbContext:DbContext
    {
        public Admin12300DbContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var abc = "server=.;database = Admin12300;uid=sa;pwd=123456;";
            optionsBuilder.UseSqlServer(abc);
        }


        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }
    }
}
