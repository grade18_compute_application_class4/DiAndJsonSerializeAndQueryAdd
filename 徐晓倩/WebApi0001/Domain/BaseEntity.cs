﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi001.Domain
{
    public abstract class BaseEntity
    {

        public BaseEntity()
        {
            IsActived = true;
            IsDeleted = false;

            CreatedTime = DateTime.Now;
            UpdateTime = DateTime.Now;
        }

        public int Id { get; set; }

        public bool IsActived { get; set; }

        public bool IsDeleted { get; set; }


        public DateTime CreatedTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public int DispalyOrder { get; set; }


        /// <summary>
        ///简介
        /// </summary>
        public string Remarks { get; set; }
    }
}
