﻿using System.Collections.Generic;
using WebApi001.Domain;

namespace WebApi0001.Domain.Entity
{
    public class Roles :BaseEntity
    {

        public string RoleName { get; set; } 

        public string Description { set; get; }

        public IEnumerable<Users> Users { get; set; }
    }
}