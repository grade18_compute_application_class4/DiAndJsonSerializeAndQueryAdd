﻿using Microsoft.AspNetCore.Authorization.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi001.Domain;

namespace WebApi0001.Domain.Entity
{
    public class Users :BaseEntity
    {

        public string UserName { get; set; }
        
        public string PassWord { get; set; }

        public int RolesId { get; set; }
        public Roles Roles { get; set; } 


    }
}
