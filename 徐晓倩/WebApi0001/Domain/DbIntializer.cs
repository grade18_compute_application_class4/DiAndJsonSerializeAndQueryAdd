﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi0001.Domain.Entity;

namespace WebApi0001.Domain
{
    public class DbIntializer
    {
        public static void Seed()
        {
            using var db = new Admin3200DbContext();
            db.Database.EnsureCreated();

            var hasUser = db.Users.Any();

            if (!hasUser)
            {
                var role = new Roles
                {
                    RoleName = "饲养员",
                    Description = "这是一个爱的饲养员"

                };
                db.Roles.Add(role);
                db.SaveChanges();

                db.Users.AddRange(new Users[]
                {
                    new Users
                    {
                        UserName = "饲养员1号",
                        PassWord="admin",
                        RolesId=role.Id
                    },
                    new Users
                    {
                        UserName = "饲养员2号",
                        PassWord="111111",
                        RolesId=role.Id

                    }

                }) ;
                
                db.SaveChanges();
            }
        }
    }
}
