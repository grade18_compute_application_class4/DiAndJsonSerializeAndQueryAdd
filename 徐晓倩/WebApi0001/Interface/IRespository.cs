﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi0001.Interface
{
    public interface IRespository <T>
    {
        IQueryable<T> Table { get; }

        T GetById(int id);


        //添加
        void Insert(T entity);

        void InsertBulk(IEnumerable<T> list);


        //更新，修改
        void Update(T entity);

        void Delete(T entity);


        //根据Id删除
        void Delete(int id);
    }
}
