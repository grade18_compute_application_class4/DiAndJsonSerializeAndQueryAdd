﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi0001.Domain.Entity;
using WebApi0001.Helper;
using WebApi0001.Interface;

namespace WebApi0001.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController :ControllerBase
    {
        
        private readonly IRespository<Users> _userReapositoty;

        public TestController(IRespository<Users> userRespository)
        {
            _userReapositoty = userRespository;
        }

        public string Get()
        {
            var list = _userReapositoty.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}
