﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi0003
{
    public class Animals
    {
        public virtual void Eat()
        {
            Console.WriteLine("动物世界");
        }
    }
    public class Dog : Animals 
    {
        public override void Eat()
        {
            Console.WriteLine("小狗狗，听话");
        }
    }

    public class Cat : Animals 
    {
        public override void Eat()
        {
            Console.WriteLine("小猫咪，咪咪咪");
        }
    }


}
