﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Domain;
using WebApi.Domain.Entity;
using WebApi.Helper;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        private readonly Admin3000 _db;

        public ValuesController(Admin3000 dbContext)
        {
            _db = dbContext;

        }


        // GET api/values
        [HttpGet]
        public string Get()
        {
            var res = _db.Users.ToList();


            return JsonHelper.SerializeObject(res);

            //var list = new List<Users>();

            //using (var db = new Admin3000())
            //{
            //    list = db.Users.ToList();
            //}

            //return list;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
