﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Interface
{
    public interface IRespository<T>
    {
        IQueryable<T> Table { get; }
        T GetbyId(int id);
       
        void Insert(T entity);

        void MoreInsert(IEnumerable<T> list);

        void Update(T entity);
        void Deleted(T entity);
        void Deleted(int id);
    }
}
