﻿using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Domain.Entity;

namespace WebApi.Domain
{
    public class DbInitializer
    {
        public static void Seed() 
        {
            using (var db = new Admin3000())
            {
                db.Database.EnsureCreated();

                if(!db.Database.EnsureCreated()){
                    var Class = new Class()
                    {
                        ClassName = "终极一班",
                        ClassType = "战士",

                    };
                    var Class1 = new Class()
                    {
                        ClassName = "终级二班",
                        ClassType = "法师",

                    };
                    db.Classes.AddRange(Class, Class1);
                    db.SaveChanges();
                    db.Users.AddRange(new Users[]
                    {
                    new Users
                    {
                        UsersName="李预期",
                        Where="河南",
                        ClassId=Class.Id
                    },
                    new Users
                    {
                        UsersName="李生殖",
                        Where="广西",
                        ClassId=Class1.Id
                    }

                    });
                    db.SaveChanges();

                }

            }

        }
    }
}
