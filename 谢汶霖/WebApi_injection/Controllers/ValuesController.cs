﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using WebApi_injection.Domain;
using WebApi_injection.Helper;

namespace WebApi_injection.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly Admin10086Context _db;
        public ValuesController(Admin10086Context admin10086Context)
        {
            _db = admin10086Context;
        }
        // GET api/values
        [HttpGet]
        public string Get()
        {
            var res = _db.Users.Include(x => x.Roles).ToList();
            return JsonHelper.SerializeObject(res);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
