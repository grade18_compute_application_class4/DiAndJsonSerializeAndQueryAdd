﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi_injection.Domain;
using WebApi_injection.Helper;
using WebApi_injection.Interface;

namespace WebApi_injection.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IPespository<Users> _userPespository;
        public TestController(IPespository<Users> userRespository)
        {
            _userPespository = userRespository;
        }

        /// <summary>
        /// 查询全部的用户信息
        /// </summary>
        /// <returns></returns>
        [Route("Get")]
        public string Get()
        {
            var list = _userPespository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }

        /// <summary>
        /// 查询指定的Id的用户信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("GetId/{id}")]
        // [HttpGet("{id}", Name = "GetById")]  => 只有一个端点输出时使用
        public string GetId(int id)
        {
            var list = _userPespository.GetById(id);

            return JsonHelper.SerializeObject(list);
        }

        [Route("DeleteId/{id}")]
        public string DeleteProduct(int id)
        {
            _userPespository.Delete(id);

            var list = _userPespository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
        [Route("insert")]
        public string Insert()
        {
            var user = new Users
            {
                Username = "路人丁",
                Password = "132654",
                RolesId = 3
            };
            _userPespository.Insert(user);
            return JsonHelper.SerializeObject(_userPespository.Table.ToList());
        }

        [Route("Update")]
        public string Update()
        {
            var user = new Users
            {
                Id = 5,
                Username = "我傻了",
                Password = "Hello word",
                RolesId = 3
            };
            _userPespository.Update(user);
            return JsonHelper.SerializeObject(_userPespository.Table.ToList());
        }
    }
}