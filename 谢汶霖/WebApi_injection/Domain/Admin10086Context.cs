﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi_injection.Domain
{
    public class Admin10086Context : DbContext
    {
        // public Admin10086Context()
        //{

        //}
        protected override void OnConfiguring(DbContextOptionsBuilder OptionsBuilder)
        {
            var connectionString = "server=.;database=Admin10086;uid=sa;pwd=123456";
            OptionsBuilder.UseSqlServer(connectionString);

            //OptionsBuilder.UseSqlServer("server=.;database=Admin10086;Trusted_Connection=True;");
        }
        //public Admin10086Context(DbContextOptions<Admin10086Context> options) : base(options)
        //{
        //   
        //}
        public DbSet<Roles> Roles { get; set; }
        public DbSet<Users> Users { get; set; }
    }
}
