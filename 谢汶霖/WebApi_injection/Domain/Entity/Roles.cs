﻿using System.Collections.Generic;

namespace WebApi_injection.Domain
{
    public class Roles:BaseEntity
    {

        public string RoleName { get; set; }
        public string Description { get; set; }
        public IEnumerable<Users> Users { get; set; }
    }
}