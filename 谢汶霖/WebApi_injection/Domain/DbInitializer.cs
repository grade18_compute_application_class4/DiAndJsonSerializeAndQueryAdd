﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi_injection.Domain
{
    public class DbInitializer
    {
        public static void seed()
        {
            using (var db = new Admin10086Context())
            {
                db.Database.EnsureCreated();
                var hasUser = db.Users.Any();
                if (!hasUser)
                {
                    var role1 = new Roles
                    {
                        RoleName = "超级管理员",
                        Description = "你是一个超级管理员"
                    };
                    var role2 = new Roles
                    {
                        RoleName = "管理员",
                        Description = "你是一个管理员"
                    };
                    var role3 = new Roles
                    {
                        RoleName = "群众",
                        Description = "你是一个群众"
                    };
                    db.Roles.Add(role1);
                    db.Roles.Add(role2);
                    db.Roles.Add(role3);
                    db.SaveChanges();

                    db.Users.AddRange(new Users[]
                    {
                        new Users
                        {
                            Username="过眼云烟",
                            Password="132654",
                            RolesId=role1.Id
                        },
                        new Users
                        {
                            Username="半卷红尘",
                            Password="123456",
                            RolesId=role2.Id
                        },
                         new Users
                        {
                            Username="路人甲",
                            Password="123456",
                            RolesId=role3.Id
                        },
                          new Users
                        {
                            Username="路人已",
                            Password="123456",
                            RolesId=role3.Id
                        }, new Users
                        {
                            Username="路人丙",
                            Password="123456",
                            RolesId=role3.Id
                        }
                    });
                    db.SaveChanges();
                }
            }
        }
    }
}
