﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi_injection.Interface
{
    public interface IPespository<T>
    {
        IQueryable<T> Table { get; }

        T GetById(int id);
        //IEnumerable<T> GetAllEntity();
        void Insert(T entity);
        void InsertBulk(IEnumerable<T> list);
        void Update(T entity);
        void Delete(T entity);
        void Delete(int id);
    }
}
