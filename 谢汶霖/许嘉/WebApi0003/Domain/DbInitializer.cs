﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi0003.Domain.Entity;

namespace WebApi0003.Domain
{
    public class DbInitializer
    {
        public static void Seed()
        {
            using var db = new Admin3200DbContext();
            db.Database.EnsureCreated();

            var hasUser = db.Users.Any();

            if (!hasUser)
            {
                     var role = new Roles
                    {
                        RoleName = "管理员",
                        Description = "管理员是我？？？"
                    };
                db.Roles.Add(role);

                db.SaveChanges();

                db.Users.AddRange(new Users[] 
                {
                    new Users
                    {
                        Username="abd",
                        Password="123456",
                        RolesId=role.Id
                    },
                     new Users
                    {
                        Username="abd01",
                        Password="abcdef",
                        RolesId=role.Id
                    }
                });
                db.SaveChanges();
            }
        }
    }
}
