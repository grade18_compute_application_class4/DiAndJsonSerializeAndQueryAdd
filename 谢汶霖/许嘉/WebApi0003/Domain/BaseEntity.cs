﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi0003.Domain
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            IsActived = true;
            IsDeled = false;
            CreateTime = DateTime.Now;
            UpdatedTime = DateTime.Now;
        }
        public int Id { get; set; }

        public bool IsActived { get; set; }

        public bool IsDeled { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdatedTime { get; set; }

        public int DisplayOrder { get; set; }

        public string Remarks { get; set; }
    }
}
