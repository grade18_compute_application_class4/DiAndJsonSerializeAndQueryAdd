﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi0003.Domain.Entity;
using WebApi0003.Helper;
using WebApi0003.Helper.imolementation;

namespace WebApi0003.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRespository<Users> _usersRepository;

        public TestController(IRespository<Users> usersRepository)
        {
            _usersRepository = usersRepository;
        }
        public string Get()
        {
            var list = _usersRepository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}