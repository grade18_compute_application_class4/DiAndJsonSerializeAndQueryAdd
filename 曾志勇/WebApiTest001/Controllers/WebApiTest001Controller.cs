﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiTest001.Domain.Entity;
using WebApiTest001.Helper;
using WebApiTest001.Implement;

namespace WebApiTest001.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WebApiTest001Controller : ControllerBase
    {
        private readonly IRespository<User> _respository;
        public WebApiTest001Controller(IRespository<User> userIRespository)
        {
            _respository = userIRespository;
        }
        public string Get()
        {
            var list = _respository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}