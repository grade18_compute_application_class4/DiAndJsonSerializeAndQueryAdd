﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using WebApiTest001.Domain;
using WebApiTest001.Domain.Entity;
using WebApiTest001.Helper;
//using Microsoft.Extensions.DependencyInjection;
namespace WebApiTest001.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        //private Admin3200DbContext db=new Admin3200DbContext();
        private readonly Admin3200DbContext _db;
        //[ActivatorUtilitiesConstructor]
        public ValuesController(Admin3200DbContext dbContext)
        {
            _db = dbContext;
        }
        // GET api/values
        [HttpGet]
        public string Get()
        {
            //return new string[] { "value1", "value2" };
           var res= _db.Users.Include(x=>x.Roles).ToList();
            //return JsonConvert.SerializeObject(res, Formatting.Indented,new JsonSerializerSettings
            //{
            //    ReferenceLoopHandling=ReferenceLoopHandling.Ignore,
            //    DateFormatString="yyy-MM-dd HH:mm:ss"
            //});
            return JsonHelper.SerializeObject(res);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
