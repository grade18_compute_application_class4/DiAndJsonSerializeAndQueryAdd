﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiTest001.Domain.Entity;

namespace WebApiTest001.Domain
{
    public class Dbinitializer
    {
        public static void seed()
        {
            using(var db=new Admin3200DbContext())
            {
                db.Database.EnsureCreated();
                var hasUsers = db.Users.Any();
                if (!hasUsers)
                {
                    var role = new Roles
                    {
                        RoleName = "管理员",
                        Description = "是一个管理员"
                    };
                    db.Roles.Add(role);
                    db.SaveChanges();
                    db.Users.AddRange(new User[]
                    {
                        new User
                        {
                            UserName="admain",
                            PassWord="123",
                            RolesId=role.id,
                        },
                        new User
                        {
                            UserName="admain02",
                            PassWord="12356",
                            RolesId=role.id,
                        }
                        
                    });
                    db.SaveChanges();
                }
            }
        }
    }
}
