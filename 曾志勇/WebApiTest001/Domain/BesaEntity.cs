﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiTest001.Domain
{
    public abstract class BesaEntity
    {
        public BesaEntity()
        {
            IsActived = true;
            IsDelete = false;
            CreateTime = DateTime.Now;
            UpdateTime = DateTime.Now;
        }
        public int id { get; set; }
        public bool IsActived { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public int DisplayOrder { get; set; }
        public  string Remarks { get; set; }
    }
}
