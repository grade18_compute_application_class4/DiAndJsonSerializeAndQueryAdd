﻿using System.Collections.Generic;

namespace WebApiTest001.Domain.Entity
{
    public class Roles:BesaEntity
    {
        public string RoleName { get; set; }

        public  string Description { get; set; }

        public IEnumerable<User> User { get; set; }
    }
}