﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi003.Domain.Entity
{
    public class Product:BaseEntity
    {
        public string ProductName { get; set; }

        public string ShortDesc { get; set; }//简介

        public string FullDesc { get; set; }//详情

        public int BrandId { get; set; }

        public Brand Brand { get; set; }
    }
}
