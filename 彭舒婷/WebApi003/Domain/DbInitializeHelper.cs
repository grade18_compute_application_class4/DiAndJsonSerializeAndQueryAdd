﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi003.Domain.Entity;

namespace WebApi003.Domain
{
    public class DbInitializeHelper
    {
        public static void Initialize()
        {
            using (var db = new Admin3200DbContext())
            {
                var dbExist = db.Database.EnsureCreated();
                //重复数据判断
                var tempBrand = db.Brands.Any();
                if (!tempBrand)
                {
                    var brand = new Brand
                    {
                        BrandName = "DIOR",
                        Description = "DIOR迪奥，" +
                        "由法国时装设计师克里斯汀·迪奥(Christian Dior)于1946年创于巴黎，" +
                        "品牌保持高贵优雅的风格品位，演绎时尚魅惑，自信活力。" +
                        "在时装、珠宝及手表、香水、彩妆和护肤领域，迪奥是优雅与奢华的完美呈现。" +
                        "在线订购免运费，正品保证。"
                    };

                    db.Brands.Add(brand);
                    db.SaveChanges();

                    db.Products.AddRange(new Product[]
                    {
                        new Product
                        {
                            ProductName="中号 DIOR BOBBY 手袋",
                            ShortDesc="蓝色 Oblique 印花",
                            FullDesc="这款 Dior Bobby 手袋为单肩包款式，彰显高雅的轮廓与协调的比例。" +
                            "这款中号手袋采用蓝色 Oblique 印花帆布精心制作，饰以复古金色五金配件提升格调。" +
                            "搭配可拆卸、可调节的肩带，点缀有军装风格搭扣，可肩背、斜挎或手提。",
                            BrandId=brand.Id
                        },
                        new Product
                        {
                            ProductName="#限量版 迪奥魅惑",
                            ShortDesc="魅惑星耀唇膏高订限量套装*",
                            FullDesc="魅惑星耀唇膏高订限量套装*，内含4款粉耀明星色，持妆闪耀，持久滋润双唇。" +
                            "经典DIOR迪奥粉高订设计，富于个性，内盖饰以化妆镜，" +
                            "套装外盒缀以象征品牌的“D”字精巧挂饰和幸运星。",
                            BrandId=brand.Id
                        }
                    });
                    db.SaveChanges();
                }
            }
        }

    }
}
