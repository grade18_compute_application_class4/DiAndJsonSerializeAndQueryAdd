﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WebApi003.Domain.Helper
{
    public class JsonHelper     //包装一下
    {
        //序列化
        public static string SerializeObject(object obj)
        {
            //json串行
            return JsonConvert.SerializeObject(obj, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    DateFormatString = "yyyy-MM-dd HH-mm-ss"
                });
        }
    }
}
