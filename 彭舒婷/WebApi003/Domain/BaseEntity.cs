﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi003.Domain
{
    public class BaseEntity
    {
        //设置默认值
        public BaseEntity()
        {
            IsActived = true;
            IsDeleted = false;
            CreatedTime = DateTime.Now;
            UpdatedTime = DateTime.Now;
        }
        public int Id { get; set; }

        public bool IsActived { get; set; }

        public bool IsDeleted { get; set; }

        public int DisplayOrder { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime UpdatedTime { get; set; }

        public string Remarks { get; set; }
    }
}
