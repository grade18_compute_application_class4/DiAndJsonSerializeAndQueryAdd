﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi003.Domain.Entity;

namespace WebApi003.Domain
{
    public class Admin3200DbContext:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=.;database=Admin3200;uid=sa;pwd=123456");
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<Brand> Brands { get; set; }
    }
}
