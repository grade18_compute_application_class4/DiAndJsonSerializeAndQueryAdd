﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebApi003.Domain;
using WebApi003.Domain.Entity;
using WebApi003.Domain.Helper;

namespace WebApi003.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        //依赖注入
        private readonly Admin3200DbContext _db;
        [ActivatorUtilitiesConstructor]
        public WeatherForecastController(Admin3200DbContext dbContext)
        {
            _db = dbContext;
        }
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        //public IEnumerable<Product> Get()
        public string Get()
        {
            //前
            //var list = new List<Product>();
            //using(var db = new Admin3200DbContext())
            //{
            //    list = db.Products.ToList();
            //}
            //return list;

            

            //var res = _db.Products.Include(x=>x.Brand).ToList();加Include(x=>x.Brand.)就会有一大串的Brand值
            var res = _db.Products.ToList();
            //json 串行
            return JsonHelper.SerializeObject(res);
        }
    }
}
