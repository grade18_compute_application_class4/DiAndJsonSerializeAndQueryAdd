﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi003.Domain.Entity;
using WebApi003.Domain.Helper;
using WebApi003.Interface;

namespace WebApi003.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IRespository<Product> _ProductIRespository;

        public ValuesController(IRespository<Product> ProductIRespository)
        {
            _ProductIRespository = ProductIRespository;
        }
        
        public string Get()
        {
            var list = _ProductIRespository.Table.ToList();
            //串行
            return JsonHelper.SerializeObject(list);
        }
    }
}
