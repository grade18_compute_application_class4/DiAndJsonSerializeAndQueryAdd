﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi003.Domain;
using WebApi003.Interface;

namespace WebApi003.Implementation
{
    public class EfRespository<T> : IRespository<T> where T : BaseEntity
    {
        private readonly Admin3200DbContext db;

        public EfRespository(Admin3200DbContext dbContext)
        {
            db = dbContext;
        }

        private DbSet<T> _entity;

        protected DbSet<T> Entity
        {
            get
            {
                return Entity;
            }
        }

        //属性
        //IQueryable可查询
        public IQueryable<T> Table
        {
            get
            {
                if(_entity==null)
                {
                    _entity = db.Set<T>();
                }
                return _entity;
            }
            
        }
        public void Delete(T entity)
        {
            this._entity.Remove(entity);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var row = Table.Where(x => x.Id == id).FirstOrDefault();
            Delete(row);
        }

        public T GetById(int id)
        {
            return _entity.Where(x => x.Id == id).FirstOrDefault();
        }

        public void Insert(T entity)
        {
            //throw new NotImplementedException();
            _entity.Add(entity);
            db.SaveChanges();
        }

        public void InsertBulk(IEnumerable<T> list)
        {
            //throw new NotImplementedException();
            _entity.AddRange(list);
            db.SaveChanges();
        }

        public void Update(T entity)
        {
            //throw new NotImplementedException();
            _entity.Update(entity);
            db.SaveChanges();
        }
    }
}
