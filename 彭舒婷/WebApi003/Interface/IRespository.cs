﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi003.Interface
{
    public interface IRespository<T>
    {
        IQueryable<T> Table { get; }

        T GetById(int id);

        void Insert(T entity);

        //批量插入
        void InsertBulk(IEnumerable<T> list);

        void Update(T entity);

        void Delete(T entity);

        //删除的重载
        void Delete(int id);
    }
}
