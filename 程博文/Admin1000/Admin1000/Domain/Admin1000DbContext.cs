﻿using Admin1000.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin1000.Domain
{
    public class Admin1000DbContext:DbContext
    {

        public Admin1000DbContext()
        {

        }

        public Admin1000DbContext(DbContextOptions<Admin1000DbContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=.;database=Admin1000;uid=sa;pwd=123456");
        }

        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }
    }
}
