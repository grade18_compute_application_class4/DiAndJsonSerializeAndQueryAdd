﻿using System.Collections.Generic;

namespace Admin1000.Domain.Entity
{
    public class Roles : BaseEntity
    {
        public  string RoleName { get; set; }

        public string Description { get; set; }

        public IEnumerable<Users> Users { get; set; }
    }
}