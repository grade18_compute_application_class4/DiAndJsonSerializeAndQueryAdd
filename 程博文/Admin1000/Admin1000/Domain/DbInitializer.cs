﻿using Admin1000.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin1000.Domain
{
    public class DbInitializer
    {
        public static void Seed()
        {
            using(Admin1000DbContext db=new Admin1000DbContext())
            {
                db.Database.EnsureCreated();


                var hasUser = db.Users.Any();

                if (!hasUser)
                {
                    var role = new Roles
                    {
                        RoleName = "管理员",
                        Description = "如你所见，这是一个管理员"
                    };

                    db.Roles.Add(role);

                    db.SaveChanges();

                    db.Users.Add(new Users
                    {
                        Username = "admin",
                        Password = "admin",
                        RolesId = role.Id
                    });

                    db.SaveChanges();

                  
                }
            }
        }
    }
}
