﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin1000
{
    public class Animals
    {
        public virtual void Eat()
        {
            Console.WriteLine("动物进食");
        }
    }
    public class Dog : Animals
    {
        public override void Eat()
        {
            Console.WriteLine("狗啃骨头");
        }
    }
    public class Cat : Animals
    {
        public override void Eat()
        {
            Console.WriteLine("猫抓老鼠");
        }
    }
}
