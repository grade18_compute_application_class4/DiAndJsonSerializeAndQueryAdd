﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin1000.Domain.Entity;
using Admin1000.Helper;
using Admin1000.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Admin1000.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRespository<Users> _userRepository;
        public TestController(IRespository<Users> userRepository)
        {
            _userRepository = userRepository;
        }
        public string Get()
        {
            var list = _userRepository.Table.ToList();
            return JsonHelper.SerializeObject(list);
            
        }
    }
}