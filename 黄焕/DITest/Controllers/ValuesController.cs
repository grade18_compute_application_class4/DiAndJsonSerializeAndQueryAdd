﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DITest.Damain;
using DITest.Damain.Entity;
using DITest.Herper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DITest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {


        private readonly Admin1100DbContext _db;

        public ValuesController(Admin1100DbContext dbContext)
        {
            _db = dbContext;

        }


        // GET api/values
        [HttpGet]
        public string Get()
        {
            var res = _db.Users.ToList();


            return JsonHerper.SerializeObject(res);

            //var list = new List<Users>();

            //using (var db = new Admin1100DbContext())
            //{
            //    list = db.Users.ToList<Users>();
            //}

            //return list;
        }

        // GET api/values/5
        //[HttpGet("{id}")]
        //public ActionResult<string> Get(int id)
        //{
            
        //}

        //// POST api/values
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
