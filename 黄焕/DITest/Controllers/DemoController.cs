﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DITest.Damain.Entity;
using DITest.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using DITest.Herper;

namespace DITest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DemoController : ControllerBase
    {
        private readonly IRespository<Users>_userRespository;

        public DemoController(IRespository<Users> userRespository)
        {
            _userRespository = userRespository;
        }

        public string Get()
        {
            var list = _userRespository.Table.ToList();
            return JsonHerper.SerializeObject(list);
        }


    }
}