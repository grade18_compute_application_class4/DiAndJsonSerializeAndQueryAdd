﻿using DITest.Damain.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DITest.Damain
{
    public class Admin1100DbContext:DbContext
    {
        public Admin1100DbContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = "server=.;database=Asmin1100;uid=sa;pwd=123456";

            optionsBuilder.UseSqlServer(connectionString);
        }

        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }

    }
}
