﻿using DITest.Damain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DITest.Damain
{
    public class DbInitializer
    {

        public static void Seed()
        {
            using (Admin1100DbContext db = new Admin1100DbContext())
            {
                db.Database.EnsureCreated();

                var hasUser = db.Users.Any();


                if (!hasUser)
                {
                    var role = new Roles
                    {
                        RoleName = "学生",
                        Description = "这是一个学生职业"

                    };
                    db.Roles.Add(role);

                    db.SaveChanges();


                    db.Users.Add(new Users
                    {
                        UserName = "小明",
                        Password = "hao123",
                        RolesId = role.Id
                    });

                    db.SaveChanges();
                    
                }
            }

            
        }
    }
}
