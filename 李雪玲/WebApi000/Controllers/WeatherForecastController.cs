﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi000.Domain;
using WebApi000.Domain.Entity;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using WebApi000.Helper;

namespace WebApi000.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly Admin5200DbContext _db;

        //[ActivatorUtilitiesConstructor]

        //public WeatherForecastController(Admin5200DbContext dbContext)
        //{
        //    _db = dbContext;
        //}

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger ,Admin5200DbContext dbContext)
        {
            _logger = logger;
            _db = dbContext;
        }

        [HttpGet]
        public string  Get()  //默认路由
        {
            //var rng = new Random();
            //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            //{
            //    Date = DateTime.Now.AddDays(index),
            //    TemperatureC = rng.Next(-20, 55),
            //    Summary = Summaries[rng.Next(Summaries.Length)]
            //})
            //.ToArray();

            var res = _db.Users.Include(x=>x.Roles).ToList();

            return JsonHelper.SerializeObject(res);
        }
    }
}
