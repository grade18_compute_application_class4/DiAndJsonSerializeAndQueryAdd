﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi000.Domain.Entity;
using WebApi000.Helper;
using WebApi000.Interface;

namespace WebApi000.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {

        private readonly IRespository<Users> _userRepository;

        public TestController(IRespository<Users> userRepository)
        {
            _userRepository = userRepository;
        }
        public string Get() //默认路由
        {
            var list = _userRepository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}