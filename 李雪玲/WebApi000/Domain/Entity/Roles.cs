﻿using System.Collections.Generic;

namespace WebApi000.Domain.Entity
{
    public class Roles : BaseEntity
    {
        public string RolesName { get; set; }

        public string Description { get; set; }

        public IEnumerable<Users> Users { get; set; }

        internal override void Update<T>(T entity)
        {
            throw new System.NotImplementedException();
        }
    }
}