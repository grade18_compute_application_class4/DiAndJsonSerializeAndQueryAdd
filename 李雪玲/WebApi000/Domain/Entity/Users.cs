﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi000.Domain.Entity
{
    public class Users : BaseEntity
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public int RolesId { get; set; }

        public Roles Roles { get; set; }

        internal override void Update<T>(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
