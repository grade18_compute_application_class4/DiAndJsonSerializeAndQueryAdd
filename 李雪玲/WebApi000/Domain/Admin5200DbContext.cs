﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi000.Domain.Entity;

namespace WebApi000.Domain
{
    public class Admin5200DbContext:DbContext
    {
        public Admin5200DbContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = "server=.;database=Admin5200;uid=sa;pwd=123456;";
            optionsBuilder.UseSqlServer(connectionString);
        }
        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }
    }
}
