﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi000.Domain
{
    public abstract class BaseEntity
    {

        public BaseEntity()
        {
            IsActived = true;
            IsDeleted = false;
            GreatedTime = DateTime.Now;
            UpdatedTime = DateTime.Now;
        }
        public int Id { get; set; }

        public bool IsActived { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime GreatedTime { get; set; }

        public DateTime UpdatedTime { get; set; }

        public string Remaks { get; set; }

        public int DisPlayOrder { get; set; }

        internal abstract void Update<T>(T entity) where T : BaseEntity;
    }
}
