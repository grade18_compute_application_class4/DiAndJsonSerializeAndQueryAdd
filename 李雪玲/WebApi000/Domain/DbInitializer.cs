﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi000.Domain.Entity;

namespace WebApi000.Domain
{
    public class DbInitializer
    {
        public static void Seed()
        {
            using var db = new Admin5200DbContext();
            db.Database.EnsureCreated();

            var hasUser = db.Users.Any();

            if (!hasUser)
            {
                var role = new Roles
                {
                    RolesName = "健康管理师",
                    Description = "其他体育健身相关"
                };

                db.Roles.Add(role);

                db.SaveChanges();

                db.Users.AddRange(new Users[]
                {
                    new Users
                    {
                        UserName = "LXueling",
                        Password = "9999",
                        RolesId = role.Id
                    },
                    new Users
                    {
                        UserName = "Lisa",
                        Password = "maimidangka",
                        RolesId = role.Id
                    }
                });

                db.SaveChanges();
            }
        }
    }
}
