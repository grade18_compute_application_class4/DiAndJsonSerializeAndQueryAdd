﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Domain.Entity;
using WebApi.Helper;
using WebApi.Interface;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRespository<Users>  _respository;
        public TestController(IRespository<Users> respository) 
        {
            _respository = respository;

        }

        public string Get() 
        {
            var list = _respository.Table.ToList();
            return JsonHelper.SerializeObject(list);
        }







    }
}
