﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Domain;
using WebApi.Domain.Entity;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using WebApi.Helper;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
     
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };


        protected readonly Admin3000 _db;
        //[ActivatorUtilitiesConstructor]
        //public WeatherForecastController(Admin3000 dbContext) 
        //{
        //    _db = dbContext;
        //}

        private readonly ILogger<WeatherForecastController> _logger;
        //
        public WeatherForecastController(ILogger<WeatherForecastController> logger,Admin3000 dbContext)
        {
            _logger = logger;
            _db = dbContext;
        }
        //
        [HttpGet]
        //public IEnumerable<Users> Get()
        //{
        //    var list = new List<Users>();
        //    using (var db=new Admin3000()) 
        //    {
        //        list = db.Users.ToList<Users>();
        //    }
        //    return list;
        //}
        public string Get() 
        {
            var list =_db.Users.Include(x=>x.Class).ToList();
            return JsonHelper.SerializeObject(list);
        }
    }
}
