﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Domain
{
    public class BaseEntity
    {
        public BaseEntity() 
        {
            CreatedTime = DateTime.Now;
            UpdateTime = DateTime.Now;
            IsActived = true;
            IsDeleted = false;
            Remarks = "Nothing";
        
        }
        public int Id { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public bool IsActived { get; set; }
        public bool IsDeleted { get; set; }
        public string Remarks { get; set; }
    }
}
