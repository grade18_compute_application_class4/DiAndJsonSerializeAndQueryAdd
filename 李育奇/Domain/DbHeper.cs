﻿using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Domain.Entity;

namespace WebApi.Domain
{
    public class DbHeper
    {
        public static void Initilize() 
        {
            using (var db = new Admin3000())
            {
              //  db.Database.EnsureCreated();
                if(!db.Database.EnsureCreated()){
                    var Class = new Class()
                    {
                        ClassName = "计算机一班",
                        ClassType = "信息与制造学院",

                    };
                    var Class1 = new Class()
                    {
                        ClassName = "电商三班",
                        ClassType = "财经商贸学院",

                    };
                    db.Classes.AddRange(Class, Class1);
                    db.SaveChanges();
                    db.Users.AddRange(new Users[]
                    {
                    new Users
                    {
                        UsersName="张三",
                        Where="广西",
                        ClassId=Class.Id
                    },
                    new Users
                    {
                        UsersName="李氏",
                        Where="江西",
                        ClassId=Class1.Id
                    }

                    });
                    db.SaveChanges();

                }

            }

        }
    }
}
