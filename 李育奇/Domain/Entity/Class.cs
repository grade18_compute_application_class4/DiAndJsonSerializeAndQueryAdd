﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Domain.Entity
{
    public class Class:BaseEntity
    {
        public string ClassName { get; set; }
        public string ClassType { get; set; }
        public IEnumerable<Users> Users { get; set; }
    }
}
