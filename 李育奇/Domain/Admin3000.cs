﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Domain.Entity;

namespace WebApi.Domain
{
    public class Admin3000:DbContext
    {
        public DbSet<Class> Classes { get; set; }
        public DbSet<Users> Users { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)  
        {
            optionsBuilder.UseSqlServer("server=.;database=Admin4000;uid=sa;pwd=123456");
        }

    }
}
